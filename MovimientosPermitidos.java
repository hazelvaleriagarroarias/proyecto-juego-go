//clase que determinara los movimientos y jugadas permitidas , necesita hablar con tablero, necesita los resultados de los metodos 

public class MovimientosPermitidos{
	
	private Tablero tablero;

	public MovimientosPermitidos(Tablero tablero){
		this.tablero = tablero;
	}// fin del metodo constructor
	
	//Este  metodo funciona para eliminar una piedra que se encuentre en una esquina
	public void espacioRodeadoEsquina(int x, int y){
		if(x==0 & y==1 || x==1 & y==0 ){ 
			if(tablero.getPiedra(0,1)!=null & tablero.getPiedra(1,0)!=null){
				if(tablero.getColorPiedra(0,1).equals("Blanca") & tablero.getColorPiedra(1,0).equals("Blanca")){	
					if(tablero.getColorPiedra(0,0)!="Blanca" ){
						tablero.eliminarPiedra(0,0);//esquina superior izquierda
					}
				}else{
					if(tablero.getColorPiedra(0,1).equals("Negra") & tablero.getColorPiedra(1,0).equals("Negra")){
						if(tablero.getColorPiedra(0,0)!="Negra" ){
							tablero.eliminarPiedra(0,0);//esquina superior izquierda
						}
					}
				}
			} 
		} //fin if superior izquierda
		if(x==0 & y==7 || x==1 & y==8 ){ 
			if(tablero.getPiedra(0,7)!=null & tablero.getPiedra(1,8)!=null){
				if(tablero.getColorPiedra(0,7).equals("Blanca") & tablero.getColorPiedra(1,8).equals("Blanca")){	
					if(tablero.getColorPiedra(0,8)!="Blanca" ){
						tablero.eliminarPiedra(0,8);//esquina superior derecha
					}
				}else{
					if(tablero.getColorPiedra(0,7).equals("Negra") & tablero.getColorPiedra(1,8).equals("Negra")){
						if(tablero.getColorPiedra(0,8)=="Blanca" ){
							tablero.eliminarPiedra(0,8);//esquina superior derecha
						}
					}
				} 
			}
		}//fin if superior derecha
		if(x==8 & y==1 || x==7 & y==0 ){ 
			if(tablero.getPiedra(8,1)!=null & tablero.getPiedra(7,0)!=null){
				if(tablero.getColorPiedra(8,1).equals("Blanca") & tablero.getColorPiedra(7,0).equals("Blanca")){
					if(tablero.getColorPiedra(8,0)!="Blanca" ){
						tablero.eliminarPiedra(8,0);//esquina inferior izquierda
					} 
				}else{
					if(tablero.getColorPiedra(8,1).equals("Negra") & tablero.getColorPiedra(7,0).equals("Negra")){
						if(tablero.getColorPiedra(8,0)!="Negra" ){
							tablero.eliminarPiedra(8,0);//esquina inferior izquierda
						} 
					}
				}	
			}
		}//fin if inferior izquierda
		if(x==7 & y==8 || x==8 & y==7 ){ 
			if(tablero.getPiedra(7,8)!=null & tablero.getPiedra(8,7)!=null){
				if (tablero.getColorPiedra(7,8).equals("Blanca") & tablero.getColorPiedra(8,7).equals("Blanca")){
					if(tablero.getColorPiedra(8,8)!="Blanca" ){
						tablero.eliminarPiedra(8,8);//esquina inferior derecha
					} 
				}else{
					if(tablero.getColorPiedra(7,8).equals("Negra") & tablero.getColorPiedra(8,7).equals("Negra")){
						if(tablero.getColorPiedra(8,8)!="Negra" ){
							tablero.eliminarPiedra(8,8);//esquina inferior derecha
						} 
					}
				}
			}
		}//fin if inferior derecha
		}	//fin metodo espacioRodeadoEsquina

	//Este  metodo funciona para eliminar una piedra que se encuentre en un borde(incompleto/por verificar)
	public void espacioRodeadoBordeIzquierdo(int x, int y){
		if(y==0){
			if(tablero.getPiedra(x+1,y+1)!=null & tablero.getPiedra(x+2,y)!=null) {
				if(tablero.getColorPiedra(x+1,y+1).equals("Negra") & tablero.getColorPiedra(x+2,y).equals("Negra") & tablero.getColorPiedra(x,y).equals("Negra")){
					if(tablero.getColorPiedra(x+1,y)!="Negra" ){
						tablero.eliminarPiedra(x+1,y);
					}
				}else{
					if(tablero.getColorPiedra(x+1,y+1).equals("Blanca") & tablero.getColorPiedra(x+2,y).equals("Blanca") & tablero.getColorPiedra(x,y).equals("Blanca")){
						if(tablero.getColorPiedra(x+1,y)!="Blanca" ){
							tablero.eliminarPiedra(x+1,y);
						}
					}
				}//Si se coloca la piedra arriba de la que se desea eliminar
			}else{
				if (tablero.getPiedra(x-1,y+1)!=null & tablero.getPiedra(x-2,y)!=null){
					if(tablero.getColorPiedra(x-1,y+1).equals("Negra") & tablero.getColorPiedra(x-2,y).equals("Negra") & tablero.getColorPiedra(x,y).equals("Negra")){
						if(tablero.getColorPiedra(x-1,y)!="Negra" ){
							tablero.eliminarPiedra(x-1,y);
						}
					}else{
						if(tablero.getColorPiedra(x-1,y+1).equals("Blanca") & tablero.getColorPiedra(x-2,y).equals("Blanca") & tablero.getColorPiedra(x,y).equals("Blanca")){
							if(tablero.getColorPiedra(x-1,y)!="Blanca" ){
								tablero.eliminarPiedra(x-1,y);
							}
						}
					}		
				}//Si se coloca la piedra abajo de la que se desea eliminar
			}
		}else{
			if (tablero.getPiedra(x-1,y-1)!=null & tablero.getPiedra(x+1,y-1)!=null){
				if(tablero.getColorPiedra(x-1,y-1).equals("Negra") & tablero.getColorPiedra(x+1,y-1).equals("Negra") & tablero.getColorPiedra(x,y).equals("Negra")){
					if(tablero.getColorPiedra(x,y-1)!="Negra" ){
						tablero.eliminarPiedra(x,y-1);
					}
				}else{
					if(tablero.getColorPiedra(x-1,y-1).equals("Blanca") & tablero.getColorPiedra(x+1,y-1).equals("Blanca") & tablero.getColorPiedra(x,y).equals("Blanca")){
						if(tablero.getColorPiedra(x,y-1)!="Blanca" ){
							tablero.eliminarPiedra(x,y-1);
						}
					}
				}
			}//Si se coloca la piedra a la derecha de la que se desea eliminar
		}
	}//fin del metodo espacioRodeadoBordeIzquierda
	
	public void espacioRodeadoBordeArriba(int x, int y){
		if(x==0){
				if(tablero.getPiedra(x,y+2)!=null & tablero.getPiedra(x+1,y+1)!=null) {
					if(tablero.getColorPiedra(x,y+2).equals("Negra") & tablero.getColorPiedra(x+1,y+1).equals("Negra") & tablero.getColorPiedra(x,y).equals("Negra")){
						if(tablero.getColorPiedra(x,y+1)!="Negra" ){
							tablero.eliminarPiedra(x,y+1);
						}
					}else{
						if(tablero.getColorPiedra(x,y+2).equals("Blanca") & tablero.getColorPiedra(x+1,y+1).equals("Blanca") & tablero.getColorPiedra(x,y).equals("Blanca")){
							if(tablero.getColorPiedra(x,y+1)!="Blanca" ){
								tablero.eliminarPiedra(x,y+1);
							}
						}
					}//Si se coloca la piedra izquierda de la que se desea eliminar
				}else{
					if(tablero.getPiedra(x+1,y-1)!=null & tablero.getPiedra(x,y-2)!=null) {
						if(tablero.getColorPiedra(x+1,y-1).equals("Negra") & tablero.getColorPiedra(x,y-2).equals("Negra") & tablero.getColorPiedra(x,y).equals("Negra")){
							if(tablero.getColorPiedra(x,y+1)!="Negra" ){
								tablero.eliminarPiedra(x,y-1);
							}
						}else{
							if(tablero.getColorPiedra(x+1,y-1).equals("Blanca") & tablero.getColorPiedra(x,y-2).equals("Blanca") & tablero.getColorPiedra(x,y).equals("Blanca")){
								if(tablero.getColorPiedra(x,y-1)!="Blanca" ){
									tablero.eliminarPiedra(x,y-1);
								}
							}
						}//Si se coloca la piedra a la derecha de la que se desea eliminar
					}
				}
		}else{
			if(tablero.getPiedra(x-1,y-1)!=null & tablero.getPiedra(x-1,y+1)!=null) {
				if(tablero.getColorPiedra(x-1,y-1).equals("Negra") & tablero.getColorPiedra(x-1,y+1).equals("Negra") & tablero.getColorPiedra(x,y).equals("Negra")){
					if(tablero.getColorPiedra(x-1,y)!="Negra" ){
						tablero.eliminarPiedra(x-1,y);
					}
				}else{
					if(tablero.getColorPiedra(x-1,y-1).equals("Blanca") & tablero.getColorPiedra(x-1,y+1).equals("Blanca") & tablero.getColorPiedra(x,y).equals("Blanca")){
						if(tablero.getColorPiedra(x-1,y)!="Blanca" ){
							tablero.eliminarPiedra(x-1,y);
						}
					}
				}
			}//Si se coloca la piedra abajo de la que se desea eliminar
		}
	}//fin del metodo espacioRodeadoBordeArriba

	public void espacioRodeadoBordeDerecho(int x, int y){
		if(y==8){
			if(tablero.getPiedra(x+1,y-1)!=null & tablero.getPiedra(x+2,y)!=null) {
				if(tablero.getColorPiedra(x+1,y-1).equals("Negra") & tablero.getColorPiedra(x+2,y).equals("Negra") & tablero.getColorPiedra(x,y).equals("Negra")){
						if(tablero.getColorPiedra(x+1,y)!="Negra" ){
							tablero.eliminarPiedra(x+1,y);
						}
					}else{
						if(tablero.getColorPiedra(x+1,y-1).equals("Blanca") & tablero.getColorPiedra(x+2,y).equals("Blanca") & tablero.getColorPiedra(x,y).equals("Blanca")){
							if(tablero.getColorPiedra(x+1,y)!="Blanca" ){
								tablero.eliminarPiedra(x+1,y);
							}
						}
					}//Si se coloca la piedra arriba de la que se desea eliminar
			}else{
				if(tablero.getPiedra(x-1,y-1)!=null & tablero.getPiedra(x-2,y)!=null) {
					if(tablero.getColorPiedra(x-1,y-1).equals("Negra") & tablero.getColorPiedra(x-2,y).equals("Negra") & tablero.getColorPiedra(x,y).equals("Negra")){
						if(tablero.getColorPiedra(x-1,y)!="Negra" ){
							tablero.eliminarPiedra(x-1,y);
						}
					}else{
						if(tablero.getColorPiedra(x-1,y-1).equals("Blanca") & tablero.getColorPiedra(x-2,y).equals("Blanca") & tablero.getColorPiedra(x,y).equals("Blanca")){
							if(tablero.getColorPiedra(x-1,y)!="Blanca" ){
								tablero.eliminarPiedra(x-1,y);
							}
						}
					}
				}
			}//Si se coloca la piedra abajo de la que se desea eliminar
		}else{
			if(tablero.getPiedra(x-1,y+1)!=null & tablero.getPiedra(x+1,y+1)!=null) {
					if(tablero.getColorPiedra(x-1,y+1).equals("Negra") & tablero.getColorPiedra(x+1,y+1).equals("Negra") & tablero.getColorPiedra(x,y).equals("Negra")){
						if(tablero.getColorPiedra(x,y+1)!="Negra" ){
							tablero.eliminarPiedra(x,y+1);
						}
					}else{
						if(tablero.getColorPiedra(x-1,y+1).equals("Blanca") & tablero.getColorPiedra(x+1,y+1).equals("Blanca") & tablero.getColorPiedra(x,y).equals("Blanca")){
							if(tablero.getColorPiedra(x,y+1)!="Blanca" ){
								tablero.eliminarPiedra(x,y+1);
							}
						}
					}
				}
		}//Si se coloca la piedra izquierda de la que se desea eliminar
	}//fin del metodo espacioRodeadoBordeDerecho
	
	public void espacioRodeadoBordeAbajo(int x, int y){
		if(x==8){
			if(tablero.getPiedra(x-1,y+1)!=null & tablero.getPiedra(x,y+2)!=null) {
				if(tablero.getColorPiedra(x-1,y+1).equals("Negra") & tablero.getColorPiedra(x,y+2).equals("Negra") & tablero.getColorPiedra(x,y).equals("Negra")){
						if(tablero.getColorPiedra(x,y+1)!="Negra" ){
							tablero.eliminarPiedra(x,y+1);
						}
					}else{
						if(tablero.getColorPiedra(x-1,y+1).equals("Blanca") & tablero.getColorPiedra(x,y+2).equals("Blanca") & tablero.getColorPiedra(x,y).equals("Blanca")){
							if(tablero.getColorPiedra(x,y+1)!="Blanca" ){
								tablero.eliminarPiedra(x,y+1);
							}
						}
					}//Si se coloca la piedra izquierda de la que se desea eliminar
			}else{
				if(tablero.getPiedra(x-1,y-1)!=null & tablero.getPiedra(x,y-2)!=null) {
					if(tablero.getColorPiedra(x-1,y-1).equals("Negra") & tablero.getColorPiedra(x,y-2).equals("Negra") & tablero.getColorPiedra(x,y).equals("Negra")){
						if(tablero.getColorPiedra(x,y-1)!="Negra" ){
							tablero.eliminarPiedra(x,y-1);
						}
					}else{
						if(tablero.getColorPiedra(x-1,y-1).equals("Blanca") & tablero.getColorPiedra(x,y-2).equals("Blanca") & tablero.getColorPiedra(x,y).equals("Blanca")){
							if(tablero.getColorPiedra(x,y-1)!="Blanca" ){
								tablero.eliminarPiedra(x,y-1);
							}
						}
					}
				}//Si se coloca la piedra a la derecha de la que se desea eliminar
			}
		}else{
			if(tablero.getPiedra(x+1,y-1)!=null & tablero.getPiedra(x+1,y+1)!=null) {
				if(tablero.getColorPiedra(x+1,y-1).equals("Negra") & tablero.getColorPiedra(x+1,y+1).equals("Negra") & tablero.getColorPiedra(x,y).equals("Negra")){
					if(tablero.getColorPiedra(x+1,y)!="Negra" ){
						tablero.eliminarPiedra(x+1,y);
					}
				}else{
					if(tablero.getColorPiedra(x+1,y-1).equals("Blanca") & tablero.getColorPiedra(x+1,y+1).equals("Blanca") & tablero.getColorPiedra(x,y).equals("Blanca")){
						if(tablero.getColorPiedra(x+1,y)!="Blanca" ){
							tablero.eliminarPiedra(x+1,y);
						}
					}
				}
			}	
		}//Si se coloca la piedra arriba de la que se desea eliminar
	}//fin del metodo espacioRodeadoBorde
	
	//Este  metodo funciona para eliminar una piedra que se encuentre en la parte central del tablero
	public void espacioRodeadoCentral(int x, int y){
		//si la piedra que se coloco esta abajo de la piedra rodeada 
		if(tablero.getPiedra(x-1,y+1)!=null & tablero.getPiedra(x-2,y)!=null || tablero.getPiedra(x-1,y)!=null & tablero.getPiedra(x-1,y-1)!=null){
			if(tablero.getColorPiedra(x-1,y+1).equals("Negra") & tablero.getColorPiedra(x-2,y).equals("Negra") & tablero.getColorPiedra(x-1,y-1).equals("Negra") & tablero.getColorPiedra(x,y).equals("Negra")){
				if(tablero.getColorPiedra(x-1,y)!="Negra" ){
					tablero.eliminarPiedra(x-1,y);
				}
			}else{
				if(tablero.getColorPiedra(x-1,y+1).equals("Blanca") & tablero.getColorPiedra(x-2,y).equals("Blanca") & tablero.getColorPiedra(x-1,y-1).equals("Negra") & tablero.getColorPiedra(x,y).equals("Blanca")){
					if(tablero.getColorPiedra(x-1,y)!="Blanca" ){
						tablero.eliminarPiedra(x-1,y);
					}
				}
			}
		}//si la piedra que se coloco esta arriba de la piedra rodeada
		if(tablero.getPiedra(x+1,y+1)!=null & tablero.getPiedra(x+1,y-1)!=null & tablero.getPiedra(x+2,y)!=null){
			if(tablero.getColorPiedra(x+1,y+1).equals("Negra") & tablero.getColorPiedra(x+1,y-1).equals("Negra") & tablero.getColorPiedra(x+2,y).equals("Negra") & tablero.getColorPiedra(x,y).equals("Negra")){
				if(tablero.getColorPiedra(x+1,y)!="Negra" ){
					tablero.eliminarPiedra(x+1,y);
				}
			}else{
				if(tablero.getColorPiedra(x+1,y+1).equals("Blanca") & tablero.getColorPiedra(x+1,y-1).equals("Blanca") & tablero.getColorPiedra(x+2,y).equals("Negra") & tablero.getColorPiedra(x,y).equals("Blanca")){
					if(tablero.getColorPiedra(x+1,y)!="Blanca" ){
						tablero.eliminarPiedra(x+1,y);
					}
				}
			}			
		}
		//si la piedra que se coloco esta al lado derecho de la piedra rodeada
		if(tablero.getPiedra(x-1,y-1)!=null & tablero.getPiedra(x,y-2)!=null & tablero.getPiedra(x+1,y-1)!=null){
			if(tablero.getColorPiedra(x-1,y-1).equals("Negra") & tablero.getColorPiedra(x,y-2).equals("Negra") & tablero.getColorPiedra(x+1,y-1).equals("Negra") & tablero.getColorPiedra(x,y).equals("Negra")){
				if(tablero.getColorPiedra(x,y-1)!="Negra" ){
					tablero.eliminarPiedra(x,y-1);
				}
			}else{
				if(tablero.getColorPiedra(x-1,y-1).equals("Blanca") & tablero.getColorPiedra(x,y-2).equals("Blanca") & tablero.getColorPiedra(x+1,y-1).equals("Negra") & tablero.getColorPiedra(x,y).equals("Blanca")){
					if(tablero.getColorPiedra(x,y-1)!="Blanca" ){
						tablero.eliminarPiedra(x,y-1);
					}
				}
			}	
		}//si la piedra que se coloco esta al lado izquierdo de la piedra rodeada
		if(tablero.getPiedra(x-1,y+1)!=null & tablero.getPiedra(x,y+2)!=null & tablero.getPiedra(x+1,y+1)!=null){
			if(tablero.getColorPiedra(x-1,y+1).equals("Negra") & tablero.getColorPiedra(x,y+2).equals("Negra") & tablero.getColorPiedra(x+1,y+1).equals("Negra") & tablero.getColorPiedra(x,y).equals("Negra")){
				if(tablero.getColorPiedra(x,y+1)!="Negra" ){
					tablero.eliminarPiedra(x,y+1);
				}
			}else{
				if(tablero.getColorPiedra(x-1,y+1).equals("Blanca") & tablero.getColorPiedra(x,y+2).equals("Blanca") & tablero.getColorPiedra(x+1,y+11).equals("Negra") & tablero.getColorPiedra(x,y).equals("Blanca")){
					if(tablero.getColorPiedra(x,y+1)!="Blanca" ){
						tablero.eliminarPiedra(x,y+1);
					}
				}
			}
		}
	}//fin del metodo espacioRodeadoCentral

	public boolean situacionKo (boolean espacioDisponible) {//no es necesario por ahora, falta desarrollarlo
		return true;
	}//fin del metodo SituacionKo

}// fin clase 
