import javax.swing.JPanel;
import javax.swing.ImageIcon;
import java.awt.Graphics;

//Este es nuestro panel

public class ImagenTablero extends JPanel{
	private GUITableroGrafico manejador;
	private ImageIcon imagenFondo, imagenPiedraUno,imagenPiedraDos,imagenPiedraTres,imagenPiedraCuatro,imagenPiedraCinco;
	
	public ImagenTablero(){
		
		imagenFondo= new ImageIcon("imagenes/imagenTablero.jpg");
		imagenPiedraUno= new ImageIcon("imagenes/PiedraBlanca.png");
		imagenPiedraDos= new ImageIcon("imagenes/PiedraBlanca.png");
		imagenPiedraTres= new ImageIcon("imagenes/PiedraBlanca.png");
		imagenPiedraCuatro= new ImageIcon("imagenes/PiedraBlanca.png");
		imagenPiedraCinco= new ImageIcon("imagenes/PiedraNegra.png");
	}//fin del metodo constructor
	
	public void paint (Graphics g){
		super.paintComponent(g);
		imagenFondo.paintIcon(null,g,0,0);
		//imagenPiedraUno.paintIcon(null,g,99,443);//abajo
		//imagenPiedraDos.paintIcon(null,g,99,396);//arriba izquierda
		imagenPiedraTres.paintIcon(null,g,146,443);//abajo
		imagenPiedraCuatro.paintIcon(null,g,146,396);//arriba derecha
		imagenPiedraCinco.paintIcon(null,g,146,490);
		imagenPiedraUno.paintIcon(null,g,99,349);
		imagenPiedraDos.paintIcon(null,g,146,537);
	}

	
}//fin de la clase ImagenTablero 
