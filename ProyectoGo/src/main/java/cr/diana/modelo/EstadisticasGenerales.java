/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.diana.modelo;

/**
 *
 * @author Dianis
 */
public class EstadisticasGenerales {
    private Partida partidas[];
    private int totalPartidasGanadasJugadorUno;
    private int totalPartidasGanadasJugadorDos;
    
    public EstadisticasGenerales(int tamano){
	partidas = new  Partida[tamano];
    }//fin del constructor con parametros
	
    public int length() {
        return partidas.length;
    }//fin del metodo length

    public void setPartida(int posicion, Partida partida) {
        partidas[posicion] = partida;
    }//fin del metodo setPartida

    public Partida getPartida(int posicion) {
        return partidas[posicion];
    }//fin del  getPartida

    public void aumentarPartidasGanadasJugadorUno() {
        totalPartidasGanadasJugadorUno++;
    }//fin del metodo aumentarPartidasGanadasJugadorUno

    public void aumentarPartidasGanadasJugadorDos() {
        totalPartidasGanadasJugadorDos++;
    }//fin del metodo aumentarPartidasGanadasJugadorDos

    public String toString() {
        String informacionPartidas = "\n\n          * Informacion de cada partida * \n", mensajeUno, mensajeDos, mensajeFinal;

        for (int indice = 0; indice < length(); indice++) {
            informacionPartidas += partidas[indice] + "\n_____________________________________\n\n";
        }// fin del for 

        //control para que la palabra "partida" se vea en plural o singular según la cantidad de partidas ganadas por el jugador uno
        if (totalPartidasGanadasJugadorUno == 1) {
            mensajeUno = "partida";
        } else {
            mensajeUno = "partidas";
        }//fin del if/else

        //control para que la palabra "partida" se vea en plural o singular según la cantidad de partidas ganadas por el jugador dos
        if (totalPartidasGanadasJugadorDos == 1) {
            mensajeDos = "partida";
        } else {
            mensajeDos = "partidas";
        }//fin del if/else

        mensajeFinal = "* * * Estadisticas Generales de las Partidas * * * "
                + "\nPartidas jugadas en total " + length()
                + "\nEl jugador uno gano un total de " + totalPartidasGanadasJugadorUno + " " + mensajeUno
                + "\nEl jugador dos gano un total de " + totalPartidasGanadasJugadorDos + " " + mensajeDos
                + informacionPartidas;

        return mensajeFinal;
    }//fin del toString

}
