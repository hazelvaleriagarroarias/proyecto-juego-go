/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.diana.modelo;

/**
 *
 * @author Dianis
 */
public class EjesTableroGrafico {

    private int arregloEjeX[];
    private int arregloEjeY[];

    public EjesTableroGrafico() {
        arregloEjeX = new int[9];
        arregloEjeY = new int[9];
    }//fin del metodo constructor

    public void sumaArregloEjeY() {
        int suma = 87;
        for (int indice = 0; indice < arregloEjeY.length; indice++) {
            suma += 30;
            arregloEjeY[indice] = suma;
        }//fin del for

    }//fin del metodo sumaArregloEjeX

    public void sumaArregloEjeX() {
        int suma = 345;
        for (int indice = 0; indice < arregloEjeX.length; indice++) {
            suma += 28;
            arregloEjeX[indice] = suma;
        }//fin del for
    }//fin del metodo sumaArregloEjeX

    public int ejeX(int posicion) {
        return arregloEjeX[posicion];
    }

    public int ejeY(int posicion) {
        return arregloEjeY[posicion];
    }
    
    public int rangoY(int click){
        int columna= -1;
        if (click > 117 & click < 147) {
            columna = 0;
        }

        if (click > 167 & click < 197) {
            columna = 1;
        }

        if (click > 209 & click < 237) {
            columna = 2;
        }

        if (click > 257 & click < 287) {
            columna = 3;
        }

        if (click > 307 & click < 338) {
            columna = 4;
        }
        if (click > 346 & click < 377) {
            columna = 5;
        }
        if (click > 396 & click < 425) {
            columna = 6;
        }
        if (click > 447 & click < 477) {
            columna = 7;
        }
        if (click > 490 & click < 517) {
            columna = 8;
        }
        return columna;
    }//fin del metodo rangoX
    
    public int rangoX(int click){
        int fila = -1;
        if (click > 373 & click < 401) {
            fila = 0;
        }

        if (click > 412 & click < 441) {
            fila = 1;
        }

        if (click > 461 & click < 489) {
            fila = 2;
        }

        if (click > 513 & click < 541) {
            fila = 3;
        }

        if (click > 552 & click < 579) {
            fila = 4;
        }
        if (click > 602 & click < 631) {
            fila = 5;
        }
        if (click > 654 & click < 680 ) {
            fila = 6;
        }
        if (click > 692 & click < 722) {
            fila = 7;
        }
        if (click > 743 & click < 771) {
            fila = 8;
        }
        return fila;
    }//fin del metodo rangoY
    
    
    
}//fin de la clase
