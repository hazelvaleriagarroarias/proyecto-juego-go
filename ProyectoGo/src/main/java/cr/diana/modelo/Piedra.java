/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.diana.modelo;

/**
 *
 * @author Dianis
 */
public class Piedra {
    private String color;
    
    public Piedra(String color) {
        this.color = color;
    }
    
    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return getColor();
    }
    
   
    
}
