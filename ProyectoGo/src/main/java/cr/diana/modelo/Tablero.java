/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.diana.modelo;

/**
 *
 * @author Dianis
 */
public class Tablero {

    private Piedra interseccion[][];

    public Tablero() {
        interseccion = new Piedra[9][9];
    }

    public void setPiedra(Piedra piedra, int fila, int columna) {
        interseccion[fila][columna] = piedra;
    }

    public Piedra getPiedra(int fila, int columna) {
        return interseccion[fila][columna];
    }

    public String getColorPiedra(int fila, int columna) {
        return interseccion[fila][columna].getColor();
    }

    public boolean comprobarEncierro(int fila, int columna) {
        if (getPiedra(fila + 1, columna) != null & getPiedra(fila, columna + 1) != null & getPiedra(fila - 1, columna) != null & getPiedra(fila, columna - 1) != null) {
            return true;
        } else {
            return false;
        }
    }

    public void eliminarPiedra(int fila, int columna) {
        interseccion[fila][columna] = null;
    }

    public int lengthFila() {
        return interseccion.length;
    }

    public int lengthColumna(int fila) {
        return interseccion[fila].length;
    }
    
    public boolean esEsquina(int fila, int columna) {//metodo para saber si la posicion es una esquina
        if (fila == 0 && columna == 1 || fila == 1 && columna == 0 || fila == 7 && columna == 0 || fila == 7 && columna == 1|| fila == 0 && columna == 7|| fila == 1 && columna == 8|| fila == 7 && columna == 8|| fila == 8 && columna == 7) {
            return true;
        } else {
            return false;
        }//fin del if/else
    }

    public boolean esBordeArriba(int fila, int columna) {
        if (fila == 0 & columna == 1 || columna == 2 || columna == 3 || columna == 4 || columna == 5 || columna == 6 || columna == 7) {
            return true;
        } else {
            if (fila == 1 & columna == 0 || columna == 1 || columna == 2 || columna == 3 || columna == 4 || columna == 5 || columna == 6 || columna == 7) {
                return true;
            } else {
                return false;
            }
        }
    }
    
    public boolean esBordeAbajo(int fila, int columna) {
        if (fila == 8 & columna == 1 || columna == 2 || columna == 3 || columna == 4 || columna == 5 || columna == 6 || columna == 7) {
            return true;
        } else {
            if (fila == 7 & columna == 0 || columna == 1 || columna == 2 || columna == 3 || columna == 4 || columna == 5 || columna == 6 || columna == 7) {
                return true;
            } else {
                return false;
            }
        }
    }
    
    public boolean esBordeIzquierda(int fila, int columna) {
        if (columna == 0 & fila == 1 || fila == 2 || fila == 3 || fila == 4 || fila == 5 || fila == 6 || fila == 7) {
            return true;
        } else {
            if (columna == 1 & fila == 0 || fila == 1 || fila == 2 || fila == 3 || fila == 4 || fila == 5 || fila == 6 || fila == 7) {
                return true;
            } else {
                return false;
            }
        }
    }


    public boolean esBordeDerecha(int fila, int columna) {//llamar cuando la esquina retorna false, para evaluar si la posicion es un borde 
        if (columna == 8 & fila == 1 || fila == 2 || fila == 3 || fila == 4 || fila == 5 || fila == 6 || fila == 7) {
            return true;
        } else {
            if (columna == 7 & fila == 0 || fila == 1 || fila == 2 || fila == 3 || fila == 4 || fila == 5 || fila == 6 || fila == 7) {
                return true;
            } else {
                return false;
            }
        }
    }

    public boolean comprobarVacio(int fila, int columna) {
        if (interseccion[fila][columna] != null) {
            return false;
        } else {
            return true;//si es true se puede colocar 
        }
    }//fin del metodo comprobarVacio	

    @Override
    public String toString() {
        String listaTablero = " ";
        for (int fila = 0; fila < interseccion.length; fila++) {
            for (int columna = 0; columna < interseccion[fila].length; columna++) {
                listaTablero += "[" + interseccion[fila][columna] + "]  ";
            }
            listaTablero += "\n";
        }//fin del for
        return listaTablero;
    }//fin del toString
}
