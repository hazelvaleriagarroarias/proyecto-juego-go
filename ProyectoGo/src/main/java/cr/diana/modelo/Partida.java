/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.diana.modelo;

/**
 *
 * @author Dianis
 */
public class Partida {
    private boolean turno;
    private String nombreJugadorUno, nombreJugadorDos, ganador;
    private int prisionerosJugadorUno, prisionerosJugadorDos;

    public Partida(boolean turno, String nombreJugadorUno, String nombreJugadorDos) {
        this.turno = turno;
        this.nombreJugadorUno = nombreJugadorUno;
        this.nombreJugadorDos = nombreJugadorDos;
    }

    public boolean getTurno() {
        return turno;
    }

    public void setTurno(boolean turno) {
        this.turno = turno;
    }

    public String getNombreJugadorUno() {
        return nombreJugadorUno;
    }

    public void setNombreJugadorUno(String nombreJugadorUno) {
        this.nombreJugadorUno = nombreJugadorUno;
    }

    public String getNombreJugadorDos() {
        return nombreJugadorDos;
    }

    public void setNombreJugadorDos(String nombreJugadorDos) {
        this.nombreJugadorDos = nombreJugadorDos;
    }

    public String getGanador() {
        return ganador;
    }

    public void setGanador(String ganador) {
        this.ganador = ganador;
    }

    public int getPrisionerosJugadorUno() {
        return prisionerosJugadorUno;
    }

    public void setPrisionerosJugadorUno(int prisionerosJugadorUno) {
        this.prisionerosJugadorUno = prisionerosJugadorUno;
    }

    public int getPrisionerosJugadorDos() {
        return prisionerosJugadorDos;
    }

    public void setPrisionerosJugadorDos(int prisionerosJugadorDos) {
        this.prisionerosJugadorDos = prisionerosJugadorDos;
    }

    @Override
    public String toString() {
        return "Partida{" + "nombreJugadorUno=" + nombreJugadorUno + ", nombreJugadorDos=" + nombreJugadorDos + ", prisionerosJugadorUno=" + prisionerosJugadorUno + ", prisionerosJugadorDos=" + prisionerosJugadorDos + '}';
    }
    
    
}
