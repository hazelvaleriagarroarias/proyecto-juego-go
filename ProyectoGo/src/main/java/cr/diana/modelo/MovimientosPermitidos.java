/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.diana.modelo;

import cr.diana.vista.JFImagenFondo;

/**
 *
 * @author Dianis
 */
public class MovimientosPermitidos {
    private Tablero tablero;
    private JFImagenFondo prisioneros;
    private Partida partida;

    public MovimientosPermitidos(Tablero tablero, JFImagenFondo prisioneros, Partida partida){
        this.tablero = tablero;
        this.prisioneros= prisioneros;
        this.partida= partida;
    }// fin del metodo constructor
	
	//Este  metodo funciona para eliminar una piedra que se encuentre en una esquina
    public void espacioRodeadoEsquina(int fila, int columna){
	if(fila==0 & columna==1 || fila==1 & columna==0 ){ 
            if(tablero.getPiedra(0,1)!=null & tablero.getPiedra(1,0)!=null){
		if(tablero.getColorPiedra(0,1).equals("Blanca") & tablero.getColorPiedra(1,0).equals("Blanca")){	
                    if(tablero.getColorPiedra(0,0)!="Blanca" ){
			tablero.eliminarPiedra(0,0);//esquina superior izquierda   
                    }
		}else{
                    if(tablero.getColorPiedra(0,1).equals("Negra") & tablero.getColorPiedra(1,0).equals("Negra")){
			if(tablero.getColorPiedra(0,0)!="Negra" ){
                            tablero.eliminarPiedra(0,0);//esquina superior izquierda
			}
                    }
		}
            } 
	} //fin if superior izquierda
	if(fila==0 & columna==7 || fila==1 & columna==8 ){ 
            if(tablero.getPiedra(0,7)!=null & tablero.getPiedra(1,8)!=null){
		if(tablero.getColorPiedra(0,7).equals("Blanca") & tablero.getColorPiedra(1,8).equals("Blanca")){	
                    if(tablero.getColorPiedra(0,8)!="Blanca" ){
			tablero.eliminarPiedra(0,8);//esquina superior derecha
                    }
		}else{
                    if(tablero.getColorPiedra(0,7).equals("Negra") & tablero.getColorPiedra(1,8).equals("Negra")){
			if(tablero.getColorPiedra(0,8)=="Blanca" ){
                            tablero.eliminarPiedra(0,8);//esquina superior derecha
			}
                    }
		} 
            }
	}//fin if superior derecha
	if(fila==8 & columna==1 || fila==7 & columna==0 ){ 
            if(tablero.getPiedra(8,1)!=null & tablero.getPiedra(7,0)!=null){
		if(tablero.getColorPiedra(8,1).equals("Blanca") & tablero.getColorPiedra(7,0).equals("Blanca")){
                    if(tablero.getColorPiedra(8,0)!="Blanca" ){
			tablero.eliminarPiedra(8,0);//esquina inferior izquierda
                    } 
		}else{
                    if(tablero.getColorPiedra(8,1).equals("Negra") & tablero.getColorPiedra(7,0).equals("Negra")){
			if(tablero.getColorPiedra(8,0)!="Negra" ){
                            tablero.eliminarPiedra(8,0);//esquina inferior izquierda
			} 
                    }
		}	
            }
	}//fin if inferior izquierda
	if(fila==7 & columna==8 || fila==8 & columna==7 ){ 
            if(tablero.getPiedra(7,8)!=null & tablero.getPiedra(8,7)!=null){
		if (tablero.getColorPiedra(7,8).equals("Blanca") & tablero.getColorPiedra(8,7).equals("Blanca")){
                    if(tablero.getColorPiedra(8,8)!="Blanca" ){
			tablero.eliminarPiedra(8,8);//esquina inferior derecha
                    } 
		}else{
                    if(tablero.getColorPiedra(7,8).equals("Negra") & tablero.getColorPiedra(8,7).equals("Negra")){
			if(tablero.getColorPiedra(8, 8) != "Negra") {
                            tablero.eliminarPiedra(8, 8);//esquina inferior derecha
                        }
                    }
                }
            }
        }//fin if inferior derecha
    }	//fin metodo espacioRodeadoEsquina

	//Este  metodo funciona para eliminar una piedra que se encuentre en un borde(incompleto/por verificar)
    public void espacioRodeadoBordeIzquierdo(int fila, int columna){
	if (columna == 0) {
            if (tablero.getPiedra(fila + 1, columna + 1) != null & tablero.getPiedra(fila + 2, columna) != null) {
                if (tablero.getColorPiedra(fila + 1, columna + 1).equals("Negra") & tablero.getColorPiedra(fila + 2, columna).equals("Negra") & tablero.getColorPiedra(fila, columna).equals("Negra")) {
                    if (tablero.getColorPiedra(fila + 1, columna) != "Negra") {
                        tablero.eliminarPiedra(fila + 1, columna);
                        
                    }
                } else {
                    if (tablero.getColorPiedra(fila + 1, columna + 1).equals("Blanca") & tablero.getColorPiedra(fila + 2, columna).equals("Blanca") & tablero.getColorPiedra(fila, columna).equals("Blanca")) {
                        if (tablero.getColorPiedra(fila + 1, columna) != "Blanca") {
                            tablero.eliminarPiedra(fila + 1, columna);
                        }
                    }
                }//Si se coloca la piedra arriba de la que se desea eliminar
            } else {
                if (tablero.getPiedra(fila - 1, columna + 1) != null & tablero.getPiedra(fila - 2, columna) != null) {
                    if (tablero.getColorPiedra(fila - 1, columna + 1).equals("Negra") & tablero.getColorPiedra(fila - 2, columna).equals("Negra") & tablero.getColorPiedra(fila, columna).equals("Negra")) {
                        if (tablero.getColorPiedra(fila - 1, columna) != "Negra") {
                            tablero.eliminarPiedra(fila - 1, columna);
                        }
                    } else {
                        if (tablero.getColorPiedra(fila - 1, columna + 1).equals("Blanca") & tablero.getColorPiedra(fila - 2, columna).equals("Blanca") & tablero.getColorPiedra(fila, columna).equals("Blanca")) {
                            if (tablero.getColorPiedra(fila - 1, columna) != "Blanca") {
                                tablero.eliminarPiedra(fila - 1, columna);
                            }
                        }
                    }
                }//Si se coloca la piedra abajo de la que se desea eliminar
            }
        } else {
            if (tablero.getPiedra(fila - 1, columna - 1) != null & tablero.getPiedra(fila + 1, columna - 1) != null) {
                if (tablero.getColorPiedra(fila - 1, columna - 1).equals("Negra") & tablero.getColorPiedra(fila + 1, columna - 1).equals("Negra") & tablero.getColorPiedra(fila, columna).equals("Negra")) {
                    if (tablero.getColorPiedra(fila, columna - 1) != "Negra") {
                        tablero.eliminarPiedra(fila, columna - 1);
                    }
                } else {
                    if (tablero.getColorPiedra(fila - 1, columna - 1).equals("Blanca") & tablero.getColorPiedra(fila + 1, columna - 1).equals("Blanca") & tablero.getColorPiedra(fila, columna).equals("Blanca")) {
                        if (tablero.getColorPiedra(fila, columna - 1) != "Blanca") {
                            tablero.eliminarPiedra(fila, columna - 1);
                        }
                    }
                }
            }//Si se coloca la piedra a la derecha de la que se desea eliminar
       	}
    }//fin del metodo espacioRodeadoBordeIzquierda
	
    public void espacioRodeadoBordeArriba(int fila, int columna){
        if (fila == 0) {
            if (tablero.getPiedra(fila, columna + 2) != null & tablero.getPiedra(fila + 1, columna + 1) != null) {
                if (tablero.getColorPiedra(fila, columna + 2).equals("Negra") & tablero.getColorPiedra(fila + 1, columna + 1).equals("Negra") & tablero.getColorPiedra(fila, columna).equals("Negra")) {
                    if (tablero.getColorPiedra(fila, columna + 1) != "Negra") {
                        tablero.eliminarPiedra(fila, columna + 1);
                    }
                } else {
                    if (tablero.getColorPiedra(fila, columna + 2).equals("Blanca") & tablero.getColorPiedra(fila + 1, columna + 1).equals("Blanca") & tablero.getColorPiedra(fila, columna).equals("Blanca")) {
                        if (tablero.getColorPiedra(fila, columna + 1) != "Blanca") {
                            tablero.eliminarPiedra(fila, columna + 1);
                        }
                    }
                }//Si se coloca la piedra izquierda de la que se desea eliminar
            } else {
                if (tablero.getPiedra(fila + 1, columna - 1) != null & tablero.getPiedra(fila, columna - 2) != null) {
                    if (tablero.getColorPiedra(fila + 1, columna - 1).equals("Negra") & tablero.getColorPiedra(fila, columna - 2).equals("Negra") & tablero.getColorPiedra(fila, columna).equals("Negra")) {
                        if (tablero.getColorPiedra(fila, columna + 1) != "Negra") {
                            tablero.eliminarPiedra(fila, columna - 1);
                        }
                    } else {
                        if (tablero.getColorPiedra(fila + 1, columna - 1).equals("Blanca") & tablero.getColorPiedra(fila, columna - 2).equals("Blanca") & tablero.getColorPiedra(fila, columna).equals("Blanca")) {
                            if (tablero.getColorPiedra(fila, columna - 1) != "Blanca") {
                                tablero.eliminarPiedra(fila, columna - 1);
                            }
                        }
                    }//Si se coloca la piedra a la derecha de la que se desea eliminar
                }
            }
        } else {
            if (tablero.getPiedra(fila - 1, columna - 1) != null & tablero.getPiedra(fila - 1, columna + 1) != null) {
                if (tablero.getColorPiedra(fila - 1, columna - 1).equals("Negra") & tablero.getColorPiedra(fila - 1, columna + 1).equals("Negra") & tablero.getColorPiedra(fila, columna).equals("Negra")) {
                    if (tablero.getColorPiedra(fila - 1, columna) != "Negra") {
                        tablero.eliminarPiedra(fila - 1, columna);
                    }
                } else {
                    if (tablero.getColorPiedra(fila - 1, columna - 1).equals("Blanca") & tablero.getColorPiedra(fila - 1, columna + 1).equals("Blanca") & tablero.getColorPiedra(fila, columna).equals("Blanca")) {
                        if (tablero.getColorPiedra(fila - 1, columna) != "Blanca") {
                            tablero.eliminarPiedra(fila - 1, columna);
                        }
                    }
                }
            }//Si se coloca la piedra abajo de la que se desea eliminar
        }
    }//fin del metodo espacioRodeadoBordeArriba

    public void espacioRodeadoBordeDerecho(int fila, int columna){
        if (columna == 8) {
            if (tablero.getPiedra(fila + 1, columna - 1) != null & tablero.getPiedra(fila + 2, columna) != null) {
                if (tablero.getColorPiedra(fila + 1, columna - 1).equals("Negra") & tablero.getColorPiedra(fila + 2, columna).equals("Negra") & tablero.getColorPiedra(fila, columna).equals("Negra")) {
                    if (tablero.getColorPiedra(fila + 1, columna) != "Negra") {
                        tablero.eliminarPiedra(fila + 1, columna);
                    }
                } else {
                    if (tablero.getColorPiedra(fila + 1, columna - 1).equals("Blanca") & tablero.getColorPiedra(fila + 2, columna).equals("Blanca") & tablero.getColorPiedra(fila, columna).equals("Blanca")) {
                        if (tablero.getColorPiedra(fila + 1, columna) != "Blanca") {
                            tablero.eliminarPiedra(fila + 1, columna);
                        }
                    }
                }//Si se coloca la piedra arriba de la que se desea eliminar
            } else {
                if (tablero.getPiedra(fila - 1, columna - 1) != null & tablero.getPiedra(fila - 2, columna) != null) {
                    if (tablero.getColorPiedra(fila - 1, columna - 1).equals("Negra") & tablero.getColorPiedra(fila - 2, columna).equals("Negra") & tablero.getColorPiedra(fila, columna).equals("Negra")) {
                        if (tablero.getColorPiedra(fila - 1, columna) != "Negra") {
                            tablero.eliminarPiedra(fila - 1, columna);
                        }
                    } else {
                        if (tablero.getColorPiedra(fila - 1, columna - 1).equals("Blanca") & tablero.getColorPiedra(fila - 2, columna).equals("Blanca") & tablero.getColorPiedra(fila, columna).equals("Blanca")) {
                            if (tablero.getColorPiedra(fila - 1, columna) != "Blanca") {
                                tablero.eliminarPiedra(fila - 1, columna);
                            }
                        }
                    }
                }
            }//Si se coloca la piedra abajo de la que se desea eliminar
        } else {
            if (tablero.getPiedra(fila - 1, columna + 1) != null & tablero.getPiedra(fila + 1, columna + 1) != null) {
                if (tablero.getColorPiedra(fila - 1, columna + 1).equals("Negra") & tablero.getColorPiedra(fila + 1, columna + 1).equals("Negra") & tablero.getColorPiedra(fila, columna).equals("Negra")) {
                    if (tablero.getColorPiedra(fila, columna + 1) != "Negra") {
                        tablero.eliminarPiedra(fila, columna + 1);
                    }
                } else {
                    if (tablero.getColorPiedra(fila - 1, columna + 1).equals("Blanca") & tablero.getColorPiedra(fila + 1, columna + 1).equals("Blanca") & tablero.getColorPiedra(fila, columna).equals("Blanca")) {
                        if (tablero.getColorPiedra(fila, columna + 1) != "Blanca") {
                            tablero.eliminarPiedra(fila, columna + 1);
                        }
                    }
                }
            }
        }//Si se coloca la piedra izquierda de la que se desea eliminar
    }//fin del metodo espacioRodeadoBordeDerecho

    public void espacioRodeadoBordeAbajo(int fila, int columna) {
        if (fila == 8) {
            if (tablero.getPiedra(fila - 1, columna + 1) != null & tablero.getPiedra(fila, columna + 2) != null) {
                if (tablero.getColorPiedra(fila - 1, columna + 1).equals("Negra") & tablero.getColorPiedra(fila, columna + 2).equals("Negra") & tablero.getColorPiedra(fila, columna).equals("Negra")) {
                    if (tablero.getColorPiedra(fila, columna + 1) != "Negra") {
                        tablero.eliminarPiedra(fila, columna + 1);
                    }
                } else {
                    if (tablero.getColorPiedra(fila - 1, columna + 1).equals("Blanca") & tablero.getColorPiedra(fila, columna + 2).equals("Blanca") & tablero.getColorPiedra(fila, columna).equals("Blanca")) {
                        if (tablero.getColorPiedra(fila, columna + 1) != "Blanca") {
                            tablero.eliminarPiedra(fila, columna + 1);
                        }
                    }
                }//Si se coloca la piedra izquierda de la que se desea eliminar
            } else {
                if (tablero.getPiedra(fila - 1, columna - 1) != null & tablero.getPiedra(fila, columna - 2) != null) {
                    if (tablero.getColorPiedra(fila - 1, columna - 1).equals("Negra") & tablero.getColorPiedra(fila, columna - 2).equals("Negra") & tablero.getColorPiedra(fila, columna).equals("Negra")) {
                        if (tablero.getColorPiedra(fila, columna - 1) != "Negra") {
                            tablero.eliminarPiedra(fila, columna - 1);
                        }
                    } else {
                        if (tablero.getColorPiedra(fila - 1, columna - 1).equals("Blanca") & tablero.getColorPiedra(fila, columna - 2).equals("Blanca") & tablero.getColorPiedra(fila, columna).equals("Blanca")) {
                            if (tablero.getColorPiedra(fila, columna - 1) != "Blanca") {
                                tablero.eliminarPiedra(fila, columna - 1);
                            }
                        }
                    }
                }//Si se coloca la piedra a la derecha de la que se desea eliminar
            }
        } else {
            if (tablero.getPiedra(fila + 1, columna - 1) != null & tablero.getPiedra(fila + 1, columna + 1) != null) {
                if (tablero.getColorPiedra(fila + 1, columna - 1).equals("Negra") & tablero.getColorPiedra(fila + 1, columna + 1).equals("Negra") & tablero.getColorPiedra(fila, columna).equals("Negra")) {
                    if (tablero.getColorPiedra(fila + 1, columna) != "Negra") {
                        tablero.eliminarPiedra(fila + 1, columna);
                    }
                } else {
                    if (tablero.getColorPiedra(fila + 1, columna - 1).equals("Blanca") & tablero.getColorPiedra(fila + 1, columna + 1).equals("Blanca") & tablero.getColorPiedra(fila, columna).equals("Blanca")) {
                        if (tablero.getColorPiedra(fila + 1, columna) != "Blanca") {
                            tablero.eliminarPiedra(fila + 1, columna);
                        }
                    }
                }
            }
        }//Si se coloca la piedra arriba de la que se desea eliminar
    }//fin del metodo espacioRodeadoBorde

    //Este  metodo funciona para eliminar una piedra que se encuentre en la parte central del tablero
    public void espacioRodeadoCentral(int fila, int columna/*,boolean turno*/) {
        //si la piedra que se coloco esta abajo de la piedra rodeada 
        if (tablero.getPiedra(fila - 1, columna + 1) != null & tablero.getPiedra(fila - 2, columna) != null || tablero.getPiedra(fila - 1, columna) != null & tablero.getPiedra(fila - 1, columna - 1) != null) {
            if (tablero.getColorPiedra(fila - 1, columna + 1).equals("Negra") & tablero.getColorPiedra(fila - 2, columna).equals("Negra") & tablero.getColorPiedra(fila - 1, columna - 1).equals("Negra") & tablero.getColorPiedra(fila, columna).equals("Negra")) {
                if (tablero.getColorPiedra(fila - 1, columna) != "Negra") {
                    tablero.eliminarPiedra(fila - 1, columna);
                  // imagenPiedra.quitarImagen(fila-1, columna, turno);
                }
            } else {
                if (tablero.getColorPiedra(fila - 1, columna + 1).equals("Blanca") & tablero.getColorPiedra(fila - 2, columna).equals("Blanca") & tablero.getColorPiedra(fila - 1, columna - 1).equals("Negra") & tablero.getColorPiedra(fila, columna).equals("Blanca")) {
                    if (tablero.getColorPiedra(fila - 1, columna) != "Blanca") {
                        tablero.eliminarPiedra(fila - 1, columna);
                       // imagenPiedra.quitarImagen(fila-1, columna, turno);
                    }
                }
            }
        }//si la piedra que se coloco esta arriba de la piedra rodeada
        if (tablero.getPiedra(fila + 1, columna + 1) != null & tablero.getPiedra(fila + 1, columna - 1) != null & tablero.getPiedra(fila + 2, columna) != null) {
            if (tablero.getColorPiedra(fila + 1, columna + 1).equals("Negra") & tablero.getColorPiedra(fila + 1, columna - 1).equals("Negra") & tablero.getColorPiedra(fila + 2, columna).equals("Negra") & tablero.getColorPiedra(fila, columna).equals("Negra")) {
                if (tablero.getColorPiedra(fila + 1, columna) != "Negra") {
                    tablero.eliminarPiedra(fila + 1, columna);
                }
            } else {
                if (tablero.getColorPiedra(fila + 1, columna + 1).equals("Blanca") & tablero.getColorPiedra(fila + 1, columna - 1).equals("Blanca") & tablero.getColorPiedra(fila + 2, columna).equals("Negra") & tablero.getColorPiedra(fila, columna).equals("Blanca")) {
                    if (tablero.getColorPiedra(fila + 1, columna) != "Blanca") {
                        tablero.eliminarPiedra(fila + 1, columna);
                    }
                }
            }
        }
        //si la piedra que se coloco esta al lado derecho de la piedra rodeada
        if (tablero.getPiedra(fila - 1, columna - 1) != null & tablero.getPiedra(fila, columna - 2) != null & tablero.getPiedra(fila + 1, columna - 1) != null) {
            if (tablero.getColorPiedra(fila - 1, columna - 1).equals("Negra") & tablero.getColorPiedra(fila, columna - 2).equals("Negra") & tablero.getColorPiedra(fila + 1, columna - 1).equals("Negra") & tablero.getColorPiedra(fila, columna).equals("Negra")) {
                if (tablero.getColorPiedra(fila, columna - 1) != "Negra") {
                    tablero.eliminarPiedra(fila, columna - 1);
                }
            } else {
                if (tablero.getColorPiedra(fila - 1, columna - 1).equals("Blanca") & tablero.getColorPiedra(fila, columna - 2).equals("Blanca") & tablero.getColorPiedra(fila + 1, columna - 1).equals("Negra") & tablero.getColorPiedra(fila, columna).equals("Blanca")) {
                    if (tablero.getColorPiedra(fila, columna - 1) != "Blanca") {
                        tablero.eliminarPiedra(fila, columna - 1);
                    }
                }
            }
        }//si la piedra que se coloco esta al lado izquierdo de la piedra rodeada
        if (tablero.getPiedra(fila - 1, columna + 1) != null & tablero.getPiedra(fila, columna + 2) != null & tablero.getPiedra(fila + 1, columna + 1) != null) {
            if (tablero.getColorPiedra(fila - 1, columna + 1).equals("Negra") & tablero.getColorPiedra(fila, columna + 2).equals("Negra") & tablero.getColorPiedra(fila + 1, columna + 1).equals("Negra") & tablero.getColorPiedra(fila, columna).equals("Negra")) {
                if (tablero.getColorPiedra(fila, columna + 1) != "Negra") {
                    tablero.eliminarPiedra(fila, columna + 1);
                }
            } else {
                if (tablero.getColorPiedra(fila - 1, columna + 1).equals("Blanca") & tablero.getColorPiedra(fila, columna + 2).equals("Blanca") & tablero.getColorPiedra(fila + 1, columna + 11).equals("Negra") & tablero.getColorPiedra(fila, columna).equals("Blanca")) {
                    if (tablero.getColorPiedra(fila, columna + 1) != "Blanca") {
                        tablero.eliminarPiedra(fila, columna + 1);
                    }
                }
            }
        }
    }//fin del metodo espacioRodeadoCentral
    
    
}//fin de la clase
