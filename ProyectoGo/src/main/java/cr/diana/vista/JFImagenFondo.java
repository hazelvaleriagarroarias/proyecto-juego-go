/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.diana.vista;

import cr.diana.controlador.Manejador;
import cr.diana.modelo.Partida;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 *
 * @author Dianis
 */
public class JFImagenFondo extends javax.swing.JFrame {

    Manejador manejador;

    /**
     * Creates new form GUIJuegoGo
     */
    public JFImagenFondo(Manejador manejador) {
        initComponents();
        this.manejador = manejador;
        addMouseListener(manejador);
        manejador.instanciarImagenes(this);
        
        // Piedras blancas
        //Primera linea
        jlUnoABlanca.setVisible(false);
        jlUnoBBlanca.setVisible(false);
        jlUnoCBlanca.setVisible(false);
        jlUnoDBlanca.setVisible(false);
        jlUnoEBlanca.setVisible(false);
        jlUnoFBlanca.setVisible(false);
        jlUnoGBlanca.setVisible(false);
        jlUnoHBlanca.setVisible(false);
        jlUnoIBlanca.setVisible(false);
        //Segunda linea
        jlDosABlanca.setVisible(false);
        jlDosBBlanca.setVisible(false);
        jlDosCBlanca.setVisible(false);
        jlDosDBlanca.setVisible(false);
        jlDosEBlanca.setVisible(false);
        jlDosFBlanca.setVisible(false);
        jlDosGBlanca.setVisible(false);
        jlDosHBlanca.setVisible(false);
        jlDosIBlanca.setVisible(false);
        //Tercera linea
        jlTresABlanca.setVisible(false);
        jlTresBBlanca.setVisible(false);
        jlTresCBlanca.setVisible(false);
        jlTresDBlanca.setVisible(false);
        jlTresEBlanca.setVisible(false);
        jlTresFBlanca.setVisible(false);
        jlTresGBlanca.setVisible(false);
        jlTresHBlanca.setVisible(false);
        jlTresIBlanca.setVisible(false);
        //Cuarta linea
        jlCuatroABlanca.setVisible(false);
        jlCuatroBBlanca.setVisible(false);
        jlCuatroCBlanca.setVisible(false);
        jlCuatroDBlanca.setVisible(false);
        jlCuatroEBlanca.setVisible(false);
        jlCuatroFBlanca.setVisible(false);
        jlCuatroGBlanca.setVisible(false);
        jlCuatroHBlanca.setVisible(false);
        jlCuatroIBlanca.setVisible(false);
        //Quinta linea
        jlCincoABlanca.setVisible(false);
        jlCincoBBlanca.setVisible(false);
        jlCincoCBlanca.setVisible(false);
        jlCincoDBlanca.setVisible(false);
        jlCincoEBlanca.setVisible(false);
        jlCincoFBlanca.setVisible(false);
        jlCincoGBlanca.setVisible(false);
        jlCincoHBlanca.setVisible(false);
        jlCincoIBlanca.setVisible(false);
        //Sexta linea
        jlSeisABlanca.setVisible(false);
        jlSeisBBlanca.setVisible(false);
        jlSeisCBlanca.setVisible(false);
        jlSeisDBlanca.setVisible(false);
        jlSeisEBlanca.setVisible(false);
        jlSeisFBlanca.setVisible(false);
        jlSeisGBlanca.setVisible(false);
        jlSeisHBlanca.setVisible(false);
        jlSeisIBlanca.setVisible(false);
        //Septima linea
        jlSieteABlanca.setVisible(false);
        jlSieteBBlanca.setVisible(false);
        jlSieteCBlanca.setVisible(false);
        jlSieteDBlanca.setVisible(false);
        jlSieteEBlanca.setVisible(false);
        jlSieteFBlanca.setVisible(false);
        jlSieteGBlanca.setVisible(false);
        jlSieteHBlanca.setVisible(false);
        jlSieteIBlanca.setVisible(false);
        //Octava linea
        jlOchoABlanca.setVisible(false);
        jlOchoBBlanca.setVisible(false);
        jlOchoCBlanca.setVisible(false);
        jlOchoDBlanca.setVisible(false);
        jlOchoEBlanca.setVisible(false);
        jlOchoFBlanca.setVisible(false);
        jlOchoGBlanca.setVisible(false);
        jlOchoHBlanca.setVisible(false);
        jlOchoIBlanca.setVisible(false);
        //Novena linea
        jlNueveABlanca.setVisible(false);
        jlNueveBBlanca.setVisible(false);
        jlNueveCBlanca.setVisible(false);
        jlNueveDBlanca.setVisible(false);
        jlNueveEBlanca.setVisible(false);
        jlNueveFBlanca.setVisible(false);
        jlNueveGBlanca.setVisible(false);
        jlNueveHBlanca.setVisible(false);
        jlNueveIBlanca.setVisible(false);

        //Piedras negras
        //Primera linea
        jlUnoANegra.setVisible(false);
        jlUnoBNegra.setVisible(false);
        jlUnoCNegra.setVisible(false);
        jlUnoENegra.setVisible(false);
        jlUnoDNegra.setVisible(false);
        jlUnoFNegra.setVisible(false);
        jlUnoGNegra.setVisible(false);
        jlUnoHNegra.setVisible(false);
        jlUnoINegra.setVisible(false);
        //Segunda linea
        jlDosANegra.setVisible(false);
        jlDosBNegra.setVisible(false);
        jlDosCNegra.setVisible(false);
        jlDosDNegra.setVisible(false);
        jlDosENegra.setVisible(false);
        jlDosFNegra.setVisible(false);
        jlDosGNegra.setVisible(false);
        jlDosHNegra.setVisible(false);
        jlDosINegra.setVisible(false);
        //Tercera linea
        jlTresANegra.setVisible(false);
        jlTresBNegra.setVisible(false);
        jlTresCNegra.setVisible(false);
        jlTresDNegra.setVisible(false);
        jlTresENegra.setVisible(false);
        jlTresFNegra.setVisible(false);
        jlTresGNegra.setVisible(false);
        jlTresHNegra.setVisible(false);
        jlTresINegra.setVisible(false);
        //Cuarta linea
        jlCuatroANegra.setVisible(false);
        jlCuatroBNegra.setVisible(false);
        jlCuatroCNegra.setVisible(false);
        jlCuatroDNegra.setVisible(false);
        jlCuatroENegra.setVisible(false);
        jlCuatroFNegra.setVisible(false);
        jlCuatroGNegra.setVisible(false);
        jlCuatroHNegra.setVisible(false);
        jlCuatroINegra.setVisible(false);
        //Quinta linea
        jlCincoANegra.setVisible(false);
        jlCincoBNegra.setVisible(false);
        jlCincoCNegra.setVisible(false);
        jlCincoDNegra.setVisible(false);
        jlCincoENegra.setVisible(false);
        jlCincoFNegra.setVisible(false);
        jlCincoGNegra.setVisible(false);
        jlCincoHNegra.setVisible(false);
        jlCincoINegra.setVisible(false);
        //Sexta linea
        jlSeisANegra.setVisible(false);
        jlSeisBNegra.setVisible(false);
        jlSeisCNegra.setVisible(false);
        jlSeisDNegra.setVisible(false);
        jlSeisENegra.setVisible(false);
        jlSeisFNegra.setVisible(false);
        jlSeisGNegra.setVisible(false);
        jlSeisHNegra.setVisible(false);
        jlSeisINegra.setVisible(false);
        //Septima linea
        jlSieteANegra.setVisible(false);
        jlSieteBNegra.setVisible(false);
        jlSieteCNegra.setVisible(false);
        jlSieteDNegra.setVisible(false);
        jlSieteENegra.setVisible(false);
        jlSieteFNegra.setVisible(false);
        jlSieteGNegra.setVisible(false);
        jlSieteHNegra.setVisible(false);
        jlSieteINegra.setVisible(false);
        //Octava linea
        jlOchoANegra.setVisible(false);
        jlOchoBNegra.setVisible(false);
        jlOchoCNegra.setVisible(false);
        jlOchoDNegra.setVisible(false);
        jlOchoENegra.setVisible(false);
        jlOchoFNegra.setVisible(false);
        jlOchoGNegra.setVisible(false);
        jlOchoHNegra.setVisible(false);
        jlOchoINegra.setVisible(false);
        //Novena linea
        jlNueveANegra.setVisible(false);
        jlNueveBNegra.setVisible(false);
        jlNueveCNegra.setVisible(false);
        jlNueveDNegra.setVisible(false);
        jlNueveENegra.setVisible(false);
        jlNueveFNegra.setVisible(false);
        jlNueveGNegra.setVisible(false);
        jlNueveHNegra.setVisible(false);
        jlNueveINegra.setVisible(false);
    }//fin del metodo constructor
    
    public void asignarImagen(int fila, int columna, Partida partida) {

        if (fila == 0) {
            if (columna == 0) {
                if (partida.getTurno()) {
                    jlUnoABlanca.setVisible(true);
                } else {
                    jlUnoANegra.setVisible(true);
                }

            } else {
                if (columna == 1) {
                    if (partida.getTurno()) {
                        jlUnoBBlanca.setVisible(true);
                    } else {
                        jlUnoBNegra.setVisible(true);
                    }
                } else {
                    if (columna == 2) {
                        if (partida.getTurno()) {
                            jlUnoCBlanca.setVisible(true);
                        } else {
                            jlUnoCNegra.setVisible(true);
                        }
                    } else {
                        if (columna == 3) {
                            if (partida.getTurno()) {
                                jlUnoDBlanca.setVisible(true);
                            } else {
                                jlUnoDNegra.setVisible(true);
                            }
                        } else {
                            if (columna == 4) {
                                if (partida.getTurno()) {
                                    jlUnoEBlanca.setVisible(true);
                                } else {
                                    jlUnoENegra.setVisible(true);
                                }
                            } else {
                                if (columna == 5) {
                                    if (partida.getTurno()) {
                                        jlUnoFBlanca.setVisible(true);
                                    } else {
                                        jlUnoFNegra.setVisible(true);
                                    }
                                } else {
                                    if (columna == 6) {
                                        if (partida.getTurno()) {
                                            jlUnoGBlanca.setVisible(true);
                                        } else {
                                            jlUnoGNegra.setVisible(true);
                                        }
                                    } else {
                                        if (columna == 7) {
                                            if (partida.getTurno()) {
                                                jlUnoHBlanca.setVisible(true);
                                            } else {
                                                jlUnoHNegra.setVisible(true);
                                            }
                                        } else {
                                            if (columna == 8) {
                                                if (partida.getTurno()) {
                                                    jlUnoIBlanca.setVisible(true);
                                                } else {
                                                    jlUnoINegra.setVisible(true);
                                                }
                                            }
                                        }//columna 8
                                    }//columna 7
                                }//columna 6
                            }//columna 5
                        }//columna 4
                    }//columna 3
                }//columna 2 
            }//columna 1
        } else {
            if (fila == 1) {
                if (columna == 0) {
                    if (partida.getTurno()) {
                        jlDosABlanca.setVisible(true);
                    } else {
                        jlDosANegra.setVisible(true);
                    }
                } else {
                    if (columna == 1) {
                        if (partida.getTurno()) {
                            jlDosBBlanca.setVisible(true);
                        } else {
                            jlDosBNegra.setVisible(true);
                        }
                    } else {
                        if (columna == 2) {
                            if (partida.getTurno()) {
                                jlDosCBlanca.setVisible(true);
                            } else {
                                jlDosCNegra.setVisible(true);
                            }
                        } else {
                            if (columna == 3) {
                                if (partida.getTurno()) {
                                    jlDosDBlanca.setVisible(true);
                                } else {
                                    jlDosDNegra.setVisible(true);
                                }
                            } else {
                                if (columna == 4) {
                                    if (partida.getTurno()) {
                                        jlDosEBlanca.setVisible(true);
                                    } else {
                                        jlDosENegra.setVisible(true);
                                    }
                                } else {
                                    if (columna == 5) {
                                        if (partida.getTurno()) {
                                            jlDosFBlanca.setVisible(true);
                                        } else {
                                            jlDosFNegra.setVisible(true);
                                        }
                                    } else {
                                        if (columna == 6) {
                                            if (partida.getTurno()) {
                                                jlDosGBlanca.setVisible(true);
                                            } else {
                                                jlDosGNegra.setVisible(true);
                                            }
                                        } else {
                                            if (columna == 7) {
                                                if (partida.getTurno()) {
                                                    jlDosHBlanca.setVisible(true);
                                                } else {
                                                    jlDosHNegra.setVisible(true);
                                                }
                                            } else {
                                                if (columna == 8) {
                                                    if (partida.getTurno()) {
                                                        jlDosIBlanca.setVisible(true);
                                                    } else {
                                                        jlDosINegra.setVisible(true);
                                                    }
                                                }
                                            }//columna 8
                                        }//columna 7
                                    }//columna 6
                                }//columna 5
                            }//columna 4
                        }//columna 3
                    }//columna 2
                }//columna 1
            } else {
                if (fila == 2) {
                    if (columna == 0) {
                        if (partida.getTurno()) {
                            jlTresABlanca.setVisible(true);
                        } else {
                            jlTresANegra.setVisible(true);
                        }
                    } else {
                        if (columna == 1) {
                            if (partida.getTurno()) {
                                jlTresBBlanca.setVisible(true);
                            } else {
                                jlTresBNegra.setVisible(true);
                            }
                        } else {
                            if (columna == 2) {
                                if (partida.getTurno()) {
                                    jlTresCBlanca.setVisible(true);
                                } else {
                                    jlTresCNegra.setVisible(true);
                                }
                            } else {
                                if (columna == 3) {
                                    if (partida.getTurno()) {
                                        jlTresDBlanca.setVisible(true);
                                    } else {
                                        jlTresDNegra.setVisible(true);
                                    }
                                } else {
                                    if (columna == 4) {
                                        if (partida.getTurno()) {
                                            jlTresEBlanca.setVisible(true);
                                        } else {
                                            jlTresENegra.setVisible(true);
                                        }
                                    } else {
                                        if (columna == 5) {
                                            if (partida.getTurno()) {
                                                jlTresFBlanca.setVisible(true);
                                            } else {
                                                jlTresFNegra.setVisible(true);
                                            }
                                        } else {
                                            if (columna == 6) {
                                                if (columna == 0) {
                                                    jlTresGBlanca.setVisible(true);
                                                } else {
                                                    jlTresGNegra.setVisible(true);
                                                }
                                            } else {
                                                if (columna == 7) {
                                                    if (partida.getTurno()) {
                                                        jlTresHBlanca.setVisible(true);
                                                    } else {
                                                        jlTresHNegra.setVisible(true);
                                                    }
                                                } else {
                                                    if (columna == 8) {
                                                        if (partida.getTurno()) {
                                                            jlTresIBlanca.setVisible(true);
                                                        } else {
                                                            jlTresINegra.setVisible(true);
                                                        }
                                                    }
                                                }//columna 8
                                            }//columna 7
                                        }//columna 6
                                    }//columna 5
                                }//columna 4
                            }//columna 3
                        }//columna 2
                    }//columna 1
                } else {
                    if (fila == 3) {
                        if (columna == 0) {
                            if (partida.getTurno()) {
                                jlCuatroABlanca.setVisible(true);
                            } else {
                                jlCuatroANegra.setVisible(true);
                            }
                        } else {
                            if (columna == 1) {
                                if (partida.getTurno()) {
                                    jlCuatroBBlanca.setVisible(true);
                                } else {
                                    jlCuatroBNegra.setVisible(true);
                                }
                            } else {
                                if (columna == 2) {
                                    if (partida.getTurno()) {
                                        jlCuatroCBlanca.setVisible(true);
                                    } else {
                                        jlCuatroCNegra.setVisible(true);
                                    }
                                } else {
                                    if (columna == 3) {
                                        if (partida.getTurno()) {
                                            jlCuatroDBlanca.setVisible(true);
                                        } else {
                                            jlCuatroDNegra.setVisible(true);
                                        }
                                    } else {
                                        if (columna == 4) {
                                            if (partida.getTurno()) {
                                                jlCuatroEBlanca.setVisible(true);
                                            } else {
                                                jlCuatroENegra.setVisible(true);
                                            }
                                        } else {
                                            if (columna == 5) {
                                                if (partida.getTurno()) {
                                                    jlCuatroFBlanca.setVisible(true);
                                                } else {
                                                    jlCuatroFNegra.setVisible(true);
                                                }
                                            } else {
                                                if (columna == 6) {
                                                    if (partida.getTurno()) {
                                                        jlCuatroGBlanca.setVisible(true);
                                                    } else {
                                                        jlCuatroGNegra.setVisible(true);
                                                    }
                                                } else {
                                                    if (columna == 7) {
                                                        if (partida.getTurno()) {
                                                            jlCuatroHBlanca.setVisible(true);
                                                        } else {
                                                            jlCuatroHNegra.setVisible(true);
                                                        }
                                                    } else {
                                                        if (columna == 8) {
                                                            if (partida.getTurno()) {
                                                                jlCuatroIBlanca.setVisible(true);
                                                            } else {
                                                                jlCuatroINegra.setVisible(true);
                                                            }
                                                        }
                                                    }//columna 8
                                                }//columna 7
                                            }//columna 6
                                        }//columna 5
                                    }//columna 4
                                }//columna 3
                            }//columna 2
                        }//columna 1
                    } else {
                        if (fila == 4) {
                            if (columna == 0) {
                                if (partida.getTurno()) {
                                    jlCincoABlanca.setVisible(true);
                                } else {
                                    jlCincoANegra.setVisible(true);
                                }
                            } else {
                                if (columna == 1) {
                                    if (partida.getTurno()) {
                                        jlCincoBBlanca.setVisible(true);
                                    } else {
                                        jlCincoBNegra.setVisible(true);
                                    }
                                } else {
                                    if (columna == 2) {
                                        if (partida.getTurno()) {
                                            jlCincoCBlanca.setVisible(true);
                                        } else {
                                            jlCincoCNegra.setVisible(true);
                                        }
                                    } else {
                                        if (columna == 3) {
                                            if (partida.getTurno()) {
                                                jlCincoDBlanca.setVisible(true);
                                            } else {
                                                jlCincoDNegra.setVisible(true);
                                            }
                                        } else {
                                            if (columna == 4) {
                                                if (partida.getTurno()) {
                                                    jlCincoEBlanca.setVisible(true);
                                                } else {
                                                    jlCincoENegra.setVisible(true);
                                                }
                                            } else {
                                                if (columna == 5) {
                                                    if (partida.getTurno()) {
                                                        jlCincoFBlanca.setVisible(true);
                                                    } else {
                                                        jlCincoFNegra.setVisible(true);
                                                    }
                                                } else {
                                                    if (columna == 6) {
                                                        if (partida.getTurno()) {
                                                            jlCincoGBlanca.setVisible(true);
                                                        } else {
                                                            jlCincoGNegra.setVisible(true);
                                                        }
                                                    } else {
                                                        if (columna == 7) {
                                                            if (partida.getTurno()) {
                                                                jlCincoHBlanca.setVisible(true);
                                                            } else {
                                                                jlCincoHNegra.setVisible(true);
                                                            }
                                                        } else {
                                                            if (columna == 8) {
                                                                if (partida.getTurno()) {
                                                                    jlCincoIBlanca.setVisible(true);
                                                                } else {
                                                                    jlCincoINegra.setVisible(true);
                                                                }
                                                            }
                                                        }//columna 8
                                                    }//columna 7
                                                }//columna 6
                                            }//columna 5
                                        }//columna 4
                                    }//columna 3
                                }//columna 2
                            }//columna 1
                        } else {
                            if (fila == 5) {
                                if (columna == 0) {
                                    if (partida.getTurno()) {
                                        jlSeisABlanca.setVisible(true);
                                    } else {
                                        jlSeisANegra.setVisible(true);
                                    }
                                } else {
                                    if (columna == 1) {
                                        if (partida.getTurno()) {
                                            jlSeisBBlanca.setVisible(true);
                                        } else {
                                            jlSeisBNegra.setVisible(true);
                                        }
                                    } else {
                                        if (columna == 2) {
                                            if (partida.getTurno()) {
                                                jlSeisCBlanca.setVisible(true);
                                            } else {
                                                jlSeisCNegra.setVisible(true);
                                            }
                                        } else {
                                            if (columna == 3) {
                                                if (partida.getTurno()) {
                                                    jlSeisDBlanca.setVisible(true);
                                                } else {
                                                    jlSeisDNegra.setVisible(true);
                                                }
                                            } else {
                                                if (columna == 4) {
                                                    if (partida.getTurno()) {
                                                        jlSeisEBlanca.setVisible(true);
                                                    } else {
                                                        jlSeisENegra.setVisible(true);
                                                    }
                                                } else {
                                                    if (columna == 5) {
                                                        if (partida.getTurno()) {
                                                            jlSeisFBlanca.setVisible(true);
                                                        } else {
                                                            jlSeisFNegra.setVisible(true);
                                                        }
                                                    } else {
                                                        if (columna == 6) {
                                                            if (partida.getTurno()) {
                                                                jlSeisGBlanca.setVisible(true);
                                                            } else {
                                                                jlSeisGNegra.setVisible(true);
                                                            }
                                                        } else {
                                                            if (columna == 7) {
                                                                if (partida.getTurno()) {
                                                                    jlSeisHBlanca.setVisible(true);
                                                                } else {
                                                                    jlSeisHNegra.setVisible(true);
                                                                }
                                                            } else {
                                                                if (columna == 8) {
                                                                    if (partida.getTurno()) {
                                                                        jlSeisIBlanca.setVisible(true);
                                                                    } else {
                                                                        jlSeisINegra.setVisible(true);
                                                                    }
                                                                }
                                                            }//columna 8
                                                        }//columna 7
                                                    }//columna 6
                                                }//columna 5
                                            }//columna 4
                                        }//columna 3
                                    }//columna 2
                                }//columna 1
                            } else {
                                if (fila == 6) {
                                    if (columna == 0) {
                                        if (partida.getTurno()) {
                                            jlSieteABlanca.setVisible(true);
                                        } else {
                                            jlSieteANegra.setVisible(true);
                                        }
                                    } else {
                                        if (columna == 1) {
                                            if (partida.getTurno()) {
                                                jlSieteBBlanca.setVisible(true);
                                            } else {
                                                jlSieteBNegra.setVisible(true);
                                            }
                                        } else {
                                            if (columna == 2) {
                                                if (partida.getTurno()) {
                                                    jlSieteCBlanca.setVisible(true);
                                                } else {
                                                    jlSieteCNegra.setVisible(true);
                                                }
                                            } else {
                                                if (columna == 3) {
                                                    if (partida.getTurno()) {
                                                        jlSieteDBlanca.setVisible(true);
                                                    } else {
                                                        jlSieteDNegra.setVisible(true);
                                                    }
                                                } else {
                                                    if (columna == 4) {
                                                        if (partida.getTurno()) {
                                                            jlSieteEBlanca.setVisible(true);
                                                        } else {
                                                            jlSieteENegra.setVisible(true);
                                                        }
                                                    } else {
                                                        if (columna == 5) {
                                                            if (partida.getTurno()) {
                                                                jlSieteFBlanca.setVisible(true);
                                                            } else {
                                                                jlSieteFNegra.setVisible(true);
                                                            }
                                                        } else {
                                                            if (columna == 6) {
                                                                if (partida.getTurno()) {
                                                                    jlSieteGBlanca.setVisible(true);
                                                                } else {
                                                                    jlSieteGNegra.setVisible(true);
                                                                }
                                                            } else {
                                                                if (columna == 7) {
                                                                    if (partida.getTurno()) {
                                                                        jlSieteHBlanca.setVisible(true);
                                                                    } else {
                                                                        jlSieteHNegra.setVisible(true);
                                                                    }
                                                                } else {
                                                                    if (columna == 8) {
                                                                        if (partida.getTurno()) {
                                                                            jlSieteIBlanca.setVisible(true);
                                                                        } else {
                                                                            jlSieteINegra.setVisible(true);
                                                                        }
                                                                    }
                                                                }//columna 8
                                                            }//columna 7
                                                        }//columna 6
                                                    }//columna 5
                                                }//columna 4
                                            }//columna3
                                        }//columna 2
                                    }//columna 1
                                } else {
                                    if (fila == 7) {
                                        if (columna == 0) {
                                            if (partida.getTurno()) {
                                                jlOchoABlanca.setVisible(true);
                                            } else {
                                                jlOchoANegra.setVisible(true);
                                            }
                                        } else {
                                            if (columna == 1) {
                                                if (partida.getTurno()) {
                                                    jlOchoBBlanca.setVisible(true);
                                                } else {
                                                    jlOchoBNegra.setVisible(true);
                                                }
                                            } else {
                                                if (columna == 2) {
                                                    if (partida.getTurno()) {
                                                        jlOchoCBlanca.setVisible(true);
                                                    } else {
                                                        jlOchoCNegra.setVisible(true);
                                                    }
                                                } else {
                                                    if (columna == 3) {
                                                        if (partida.getTurno()) {
                                                            jlOchoDBlanca.setVisible(true);
                                                        } else {
                                                            jlOchoDNegra.setVisible(true);
                                                        }
                                                    } else {
                                                        if (columna == 4) {
                                                            if (partida.getTurno()) {
                                                                jlOchoEBlanca.setVisible(true);
                                                            } else {
                                                                jlOchoENegra.setVisible(true);
                                                            }
                                                        } else {
                                                            if (columna == 5) {
                                                                if (partida.getTurno()) {
                                                                    jlOchoFBlanca.setVisible(true);
                                                                } else {
                                                                    jlOchoFNegra.setVisible(true);
                                                                }
                                                            } else {
                                                                if (columna == 6) {
                                                                    if (partida.getTurno()) {
                                                                        jlOchoGBlanca.setVisible(true);
                                                                    } else {
                                                                        jlOchoGNegra.setVisible(true);
                                                                    }
                                                                } else {
                                                                    if (columna == 7) {
                                                                        if (partida.getTurno()) {
                                                                            jlOchoHBlanca.setVisible(true);
                                                                        } else {
                                                                            jlOchoHNegra.setVisible(true);
                                                                        }
                                                                    } else {
                                                                        if (columna == 8) {
                                                                            if (partida.getTurno()) {
                                                                                jlOchoIBlanca.setVisible(true);
                                                                            } else {
                                                                                jlOchoINegra.setVisible(true);
                                                                            }
                                                                        }
                                                                    }//columna 8
                                                                }//columna 7
                                                            }//columna 6
                                                        }//columna 5
                                                    }//columna 4
                                                }//columna 3
                                            }//columna 2
                                        }//columna 1
                                    } else {
                                        if (fila == 8) {
                                            if (columna == 0) {
                                                if (partida.getTurno()) {
                                                    jlNueveABlanca.setVisible(true);
                                                } else {
                                                    jlNueveANegra.setVisible(true);
                                                }
                                            } else {
                                                if (columna == 1) {
                                                    if (partida.getTurno()) {
                                                        jlNueveBBlanca.setVisible(true);
                                                    } else {
                                                        jlNueveBNegra.setVisible(true);
                                                    }
                                                } else {
                                                    if (columna == 2) {
                                                        if (partida.getTurno()) {
                                                            jlNueveCBlanca.setVisible(true);
                                                        } else {
                                                            jlNueveCNegra.setVisible(true);
                                                        }
                                                    } else {
                                                        if (columna == 3) {
                                                            if (partida.getTurno()) {
                                                                jlNueveDBlanca.setVisible(true);
                                                            } else {
                                                                jlNueveDNegra.setVisible(true);
                                                            }
                                                        } else {
                                                            if (columna == 4) {
                                                                if (partida.getTurno()) {
                                                                    jlNueveEBlanca.setVisible(true);
                                                                } else {
                                                                    jlNueveENegra.setVisible(true);
                                                                }
                                                            } else {
                                                                if (columna == 5) {
                                                                    if (partida.getTurno()) {
                                                                        jlNueveFBlanca.setVisible(true);
                                                                    } else {
                                                                        jlNueveFNegra.setVisible(true);
                                                                    }
                                                                } else {
                                                                    if (columna == 6) {
                                                                        if (partida.getTurno()) {
                                                                            jlNueveGBlanca.setVisible(true);
                                                                        } else {
                                                                            jlNueveGNegra.setVisible(true);
                                                                        }
                                                                    } else {
                                                                        if (columna == 7) {
                                                                            if (partida.getTurno()) {
                                                                                jlNueveHBlanca.setVisible(true);
                                                                            } else {
                                                                                jlNueveHNegra.setVisible(true);
                                                                            }
                                                                        } else {
                                                                            if (columna == 8) {
                                                                                if (partida.getTurno()) {
                                                                                    jlNueveIBlanca.setVisible(true);
                                                                                } else {
                                                                                    jlNueveINegra.setVisible(true);
                                                                                }
                                                                            }
                                                                        }//columna 8 
                                                                    }//columna 7
                                                                }//columna 6
                                                            }//columna 5
                                                        }//columna 4
                                                    }//columna 3
                                                }//columna 2
                                            }//columna 1
                                        }//if fila 8
                                    }//else fila 8
                                }//else fila 7
                            }//else fila 6
                        }//else fila 5
                    }//else fila 4
                }//else fila 3
            }//else fila 2
        }//else fila 1
    }//fin del metodo asignar imagen
    
    public JLabel quitarImagen(int fila, int columna, boolean turno) {

            if (fila == 0) {
                if (columna == 0) {
                    if (!turno) {
                        return jlUnoABlanca;
                    } else {
                        return jlUnoANegra;
                    }

                } else {
                    if (columna == 1) {
                        if (!turno) {
                            return jlUnoBBlanca;
                        } else {
                            return jlUnoBNegra;
                        }
                    } else {
                        if (columna == 2) {
                            if (!turno) {
                               return jlUnoCBlanca;
                            } else {
                               return jlUnoCNegra;
                            }
                        } else {
                            if (columna == 3) {
                                if (!turno) {
                                   return jlUnoDBlanca;
                                } else {
                                   return jlUnoENegra;
                                }
                            } else {
                                if (columna == 4) {
                                    if (!turno) {
                                       return jlUnoEBlanca;
                                    } else {
                                        return jlUnoDNegra;
                                    }
                                } else {
                                    if (columna == 5) {
                                        if (!turno) {
                                           return jlUnoFBlanca;
                                        } else {
                                            return jlUnoFNegra;
                                        }
                                    } else {
                                        if (columna == 6) {
                                            if (!turno) {
                                                return jlUnoGBlanca;
                                            } else {
                                                return jlUnoGNegra;
                                            }
                                        } else {
                                            if (columna == 7) {
                                                if (!turno) {
                                                    return jlUnoHBlanca;
                                                } else {
                                                    return jlUnoHNegra;
                                                }
                                            } else {
                                                if (columna == 8) {
                                                    if (!turno) {
                                                        return jlUnoIBlanca;
                                                    } else {
                                                        return jlUnoINegra;
                                                    }
                                                }
                                            }//columna 8
                                        }//columna 7
                                    }//columna 6
                                }//columna 5
                            }//columna 4
                        }//columna 3
                    }//columna 2 
                }//columna 1
            } else {
                if (fila == 1) {
                    if (columna == 0) {
                        if (turno) {
                            jlDosABlanca.setVisible(false);
                        } else {
                            jlDosANegra.setVisible(false);
                        }
                    } else {
                        if (columna == 1) {
                            if (turno) {
                                jlDosBBlanca.setVisible(false);
                            } else {
                                jlDosBNegra.setVisible(false);
                            }
                        } else {
                            if (columna == 2) {
                                if (turno) {
                                    jlDosCBlanca.setVisible(false);
                                } else {
                                    jlDosCNegra.setVisible(false);
                                }
                            } else {
                                if (columna == 3) {
                                    if (turno) {
                                        jlDosDBlanca.setVisible(false);
                                    } else {
                                        jlDosDNegra.setVisible(false);
                                    }
                                } else {
                                    if (columna == 4) {
                                        if (turno) {
                                            jlDosEBlanca.setVisible(false);
                                        } else {
                                            jlDosENegra.setVisible(false);
                                        }
                                    } else {
                                        if (columna == 5) {
                                            if (turno) {
                                                jlDosFBlanca.setVisible(false);
                                            } else {
                                                jlDosFNegra.setVisible(false);
                                            }
                                        } else {
                                            if (columna == 6) {
                                                if (turno) {
                                                    jlDosGBlanca.setVisible(false);
                                                } else {
                                                    jlDosGNegra.setVisible(false);
                                                }
                                            } else {
                                                if (columna == 7) {
                                                    if (turno) {
                                                        jlDosHBlanca.setVisible(false);
                                                    } else {
                                                        jlDosHNegra.setVisible(false);
                                                    }
                                                } else {
                                                    if (columna == 8) {
                                                        if (turno) {
                                                            jlDosIBlanca.setVisible(false);
                                                        } else {
                                                            jlDosINegra.setVisible(false);
                                                        }
                                                    }
                                                }//columna 8
                                            }//columna 7
                                        }//columna 6
                                    }//columna 5
                                }//columna 4
                            }//columna 3
                        }//columna 2
                    }//columna 1
                } else {
                    if (fila == 2) {
                        if (columna == 0) {
                            if (turno) {
                                jlTresABlanca.setVisible(false);
                            } else {
                                jlTresANegra.setVisible(false);
                            }
                        } else {
                            if (columna == 1) {
                                if (turno) {
                                    jlTresBBlanca.setVisible(false);
                                } else {
                                    jlTresBNegra.setVisible(false);
                                }
                            } else {
                                if (columna == 2) {
                                    if (turno) {
                                        jlTresCBlanca.setVisible(false);
                                    } else {
                                        jlTresCNegra.setVisible(false);
                                    }
                                } else {
                                    if (columna == 3) {
                                        if (turno) {
                                            jlTresDBlanca.setVisible(false);
                                        } else {
                                            jlTresDNegra.setVisible(false);
                                        }
                                    } else {
                                        if (columna == 4) {
                                            if (turno) {
                                                jlTresEBlanca.setVisible(false);
                                            } else {
                                                jlTresENegra.setVisible(false);
                                            }
                                        } else {
                                            if (columna == 5) {
                                                if (turno) {
                                                    jlTresFBlanca.setVisible(false);
                                                } else {
                                                    jlTresFNegra.setVisible(false);
                                                }
                                            } else {
                                                if (columna == 6) {
                                                    if (turno) {
                                                        jlTresGBlanca.setVisible(false);
                                                    } else {
                                                        jlTresGNegra.setVisible(false);
                                                    }
                                                } else {
                                                    if (columna == 7) {
                                                        if (turno) {
                                                            jlTresHBlanca.setVisible(false);
                                                        } else {
                                                            jlTresHNegra.setVisible(false);
                                                        }
                                                    } else {
                                                        if (columna == 8) {
                                                            if (turno) {
                                                                jlTresIBlanca.setVisible(false);
                                                            } else {
                                                                jlTresINegra.setVisible(false);
                                                            }
                                                        }
                                                    }//columna 8
                                                }//columna 7
                                            }//columna 6
                                        }//columna 5
                                    }//columna 4
                                }//columna 3
                            }//columna 2
                        }//columna 1
                    } else {
                        if (fila == 3) {
                            if (columna == 0) {
                                if (turno) {
                                    jlCuatroABlanca.setVisible(false);
                                } else {
                                    jlCuatroANegra.setVisible(false);
                                }
                            } else {
                                if (columna == 1) {
                                    if (turno) {
                                        jlCuatroBBlanca.setVisible(false);
                                    } else {
                                        jlCuatroBNegra.setVisible(false);
                                    }
                                } else {
                                    if (columna == 2) {
                                        if (turno) {
                                            jlCuatroCBlanca.setVisible(false);
                                        } else {
                                            jlCuatroCNegra.setVisible(false);
                                        }
                                    } else {
                                        if (columna == 3) {
                                            if (turno) {
                                                jlCuatroDBlanca.setVisible(false);
                                            } else {
                                                jlCuatroDNegra.setVisible(false);
                                            }
                                        } else {
                                            if (columna == 4) {
                                                if (turno) {
                                                    jlCuatroEBlanca.setVisible(false);
                                                } else {
                                                    jlCuatroENegra.setVisible(false);
                                                }
                                            } else {
                                                if (columna == 5) {
                                                    if (turno) {
                                                        jlCuatroFBlanca.setVisible(false);
                                                    } else {
                                                        jlCuatroFNegra.setVisible(false);
                                                    }
                                                } else {
                                                    if (columna == 6) {
                                                        if (turno) {
                                                            jlCuatroGBlanca.setVisible(false);
                                                        } else {
                                                            jlCuatroGNegra.setVisible(false);
                                                        }
                                                    } else {
                                                        if (columna == 7) {
                                                            if (turno) {
                                                                jlCuatroHBlanca.setVisible(false);
                                                            } else {
                                                                jlCuatroHNegra.setVisible(false);
                                                            }
                                                        } else {
                                                            if (columna == 8) {
                                                                if (turno) {
                                                                    jlCuatroIBlanca.setVisible(false);
                                                                } else {
                                                                    jlCuatroINegra.setVisible(false);
                                                                }
                                                            }
                                                        }//columna 8
                                                    }//columna 7
                                                }//columna 6
                                            }//columna 5
                                        }//columna 4
                                    }//columna 3
                                }//columna 2
                            }//columna 1
                        } else {
                            if (fila == 4) {
                                if (columna == 0) {
                                    if (turno) {
                                        jlCincoABlanca.setVisible(false);
                                    } else {
                                        jlCincoANegra.setVisible(false);
                                    }
                                } else {
                                    if (columna == 1) {
                                        if (turno) {
                                            jlCincoBBlanca.setVisible(false);
                                        } else {
                                            jlCincoBNegra.setVisible(false);
                                        }
                                    } else {
                                        if (columna == 2) {
                                            if (turno) {
                                                jlCincoCBlanca.setVisible(false);
                                            } else {
                                                jlCincoCNegra.setVisible(false);
                                            }
                                        } else {
                                            if (columna == 3) {
                                                if (turno) {
                                                    jlCincoDBlanca.setVisible(false);
                                                } else {
                                                    jlCincoDNegra.setVisible(false);
                                                }
                                            } else {
                                                if (columna == 4) {
                                                    if (turno) {
                                                        jlCincoEBlanca.setVisible(false);
                                                    } else {
                                                        jlCincoENegra.setVisible(false);
                                                    }
                                                } else {
                                                    if (columna == 5) {
                                                        if (turno) {
                                                            jlCincoFBlanca.setVisible(false);
                                                        } else {
                                                            jlCincoFNegra.setVisible(false);
                                                        }
                                                    } else {
                                                        if (columna == 6) {
                                                            if (turno) {
                                                                jlCincoGBlanca.setVisible(false);
                                                            } else {
                                                                jlCincoGNegra.setVisible(false);
                                                            }
                                                        } else {
                                                            if (columna == 7) {
                                                                if (turno) {
                                                                    jlCincoHBlanca.setVisible(false);
                                                                } else {
                                                                    jlCincoHNegra.setVisible(false);
                                                                }
                                                            } else {
                                                                if (columna == 8) {
                                                                    if (turno) {
                                                                        jlCincoIBlanca.setVisible(false);
                                                                    } else {
                                                                        jlCincoINegra.setVisible(false);
                                                                    }
                                                                }
                                                            }//columna 8
                                                        }//columna 7
                                                    }//columna 6
                                                }//columna 5
                                            }//columna 4
                                        }//columna 3
                                    }//columna 2
                                }//columna 1
                            } else {
                                if (fila == 5) {
                                    if (columna == 0) {
                                        if (turno) {
                                            jlSeisABlanca.setVisible(false);
                                        } else {
                                            jlSeisANegra.setVisible(false);
                                        }
                                    } else {
                                        if (columna == 1) {
                                            if (turno) {
                                                jlSeisBBlanca.setVisible(false);
                                            } else {
                                                jlSeisBNegra.setVisible(false);
                                            }
                                        } else {
                                            if (columna == 2) {
                                                if (turno) {
                                                    jlSeisCBlanca.setVisible(false);
                                                } else {
                                                    jlSeisCNegra.setVisible(false);
                                                }
                                            } else {
                                                if (columna == 3) {
                                                    if (turno) {
                                                        jlSeisDBlanca.setVisible(false);
                                                    } else {
                                                        jlSeisDNegra.setVisible(false);
                                                    }
                                                } else {
                                                    if (columna == 4) {
                                                        if (turno) {
                                                            jlSeisEBlanca.setVisible(false);
                                                        } else {
                                                            jlSeisENegra.setVisible(false);
                                                        }
                                                    } else {
                                                        if (columna == 5) {
                                                            if (turno) {
                                                                jlSeisFBlanca.setVisible(false);
                                                            } else {
                                                                jlSeisFNegra.setVisible(false);
                                                            }
                                                        } else {
                                                            if (columna == 6) {
                                                                if (turno) {
                                                                    jlSeisGBlanca.setVisible(false);
                                                                } else {
                                                                    jlSeisGNegra.setVisible(false);
                                                                }
                                                            } else {
                                                                if (columna == 7) {
                                                                    if (turno) {
                                                                        jlSeisHBlanca.setVisible(false);
                                                                    } else {
                                                                        jlSeisHNegra.setVisible(false);
                                                                    }
                                                                } else {
                                                                    if (columna == 8) {
                                                                        if (turno) {
                                                                            jlSeisIBlanca.setVisible(false);
                                                                        } else {
                                                                            jlSeisINegra.setVisible(false);
                                                                        }
                                                                    }
                                                                }//columna 8
                                                            }//columna 7
                                                        }//columna 6
                                                    }//columna 5
                                                }//columna 4
                                            }//columna 3
                                        }//columna 2
                                    }//columna 1
                                } else {
                                    if (fila == 6) {
                                        if (columna == 0) {
                                            if (turno) {
                                                jlSieteABlanca.setVisible(false);
                                            } else {
                                                jlSieteANegra.setVisible(false);
                                            }
                                        } else {
                                            if (columna == 1) {
                                                if (turno) {
                                                    jlSieteBBlanca.setVisible(false);
                                                } else {
                                                    jlSieteBNegra.setVisible(false);
                                                }
                                            } else {
                                                if (columna == 2) {
                                                    if (turno) {
                                                        jlSieteCBlanca.setVisible(false);
                                                    } else {
                                                        jlSieteCNegra.setVisible(false);
                                                    }
                                                } else {
                                                    if (columna == 3) {
                                                        if (turno) {
                                                            jlSieteDBlanca.setVisible(false);
                                                        } else {
                                                            jlSieteDNegra.setVisible(false);
                                                        }
                                                    } else {
                                                        if (columna == 4) {
                                                            if (turno) {
                                                                jlSieteEBlanca.setVisible(false);
                                                            } else {
                                                                jlSieteENegra.setVisible(false);
                                                            }
                                                        } else {
                                                            if (columna == 5) {
                                                                if (turno) {
                                                                    jlSieteFBlanca.setVisible(false);
                                                                } else {
                                                                    jlSieteFNegra.setVisible(false);
                                                                }
                                                            } else {
                                                                if (columna == 6) {
                                                                    if (turno) {
                                                                        jlSieteGBlanca.setVisible(false);
                                                                    } else {
                                                                        jlSieteGNegra.setVisible(false);
                                                                    }
                                                                } else {
                                                                    if (columna == 7) {
                                                                        if (turno) {
                                                                            jlSieteHBlanca.setVisible(false);
                                                                        } else {
                                                                            jlSieteHNegra.setVisible(false);
                                                                        }
                                                                    } else {
                                                                        if (columna == 8) {
                                                                            if (turno) {
                                                                                jlSieteIBlanca.setVisible(false);
                                                                            } else {
                                                                                jlSieteINegra.setVisible(false);
                                                                            }
                                                                        }
                                                                    }//columna 8
                                                                }//columna 7
                                                            }//columna 6
                                                        }//columna 5
                                                    }//columna 4
                                                }//columna3
                                            }//columna 2
                                        }//columna 1
                                    } else {
                                        if (fila == 7) {
                                            if (columna == 0) {
                                                if (turno) {
                                                    jlOchoABlanca.setVisible(false);
                                                } else {
                                                    jlOchoANegra.setVisible(false);
                                                }
                                            } else {
                                                if (columna == 1) {
                                                    if (turno) {
                                                        jlOchoBBlanca.setVisible(false);
                                                    } else {
                                                        jlOchoBNegra.setVisible(false);
                                                    }
                                                } else {
                                                    if (columna == 2) {
                                                        if (turno) {
                                                            jlOchoCBlanca.setVisible(false);
                                                        } else {
                                                            jlOchoCNegra.setVisible(false);
                                                        }
                                                    } else {
                                                        if (columna == 3) {
                                                            if (turno) {
                                                                jlOchoDBlanca.setVisible(false);
                                                            } else {
                                                                jlOchoDNegra.setVisible(false);
                                                            }
                                                        } else {
                                                            if (columna == 4) {
                                                                if (turno) {
                                                                    jlOchoEBlanca.setVisible(false);
                                                                } else {
                                                                    jlOchoENegra.setVisible(false);
                                                                }
                                                            } else {
                                                                if (columna == 5) {
                                                                    if (turno) {
                                                                        jlOchoFBlanca.setVisible(false);
                                                                    } else {
                                                                        jlOchoFNegra.setVisible(false);
                                                                    }
                                                                } else {
                                                                    if (columna == 6) {
                                                                        if (turno) {
                                                                            jlOchoGBlanca.setVisible(false);
                                                                        } else {
                                                                            jlOchoGNegra.setVisible(false);
                                                                        }
                                                                    } else {
                                                                        if (columna == 7) {
                                                                            if (turno) {
                                                                                jlOchoHBlanca.setVisible(false);
                                                                            } else {
                                                                                jlOchoHNegra.setVisible(false);
                                                                            }
                                                                        } else {
                                                                            if (columna == 8) {
                                                                                if (turno) {
                                                                                    jlOchoIBlanca.setVisible(false);
                                                                                } else {
                                                                                    jlOchoINegra.setVisible(false);
                                                                                }
                                                                            }
                                                                        }//columna 8
                                                                    }//columna 7
                                                                }//columna 6
                                                            }//columna 5
                                                        }//columna 4
                                                    }//columna 3
                                                }//columna 2
                                            }//columna 1
                                        } else {
                                            if (fila == 8) {
                                                if (columna == 0) {
                                                    if (turno) {
                                                        jlNueveABlanca.setVisible(false);
                                                    } else {
                                                        jlNueveANegra.setVisible(false);
                                                    }
                                                } else {
                                                    if (columna == 1) {
                                                        if (turno) {
                                                            jlNueveBBlanca.setVisible(false);
                                                        } else {
                                                            jlNueveBNegra.setVisible(false);
                                                        }
                                                    } else {
                                                        if (columna == 2) {
                                                            if (turno) {
                                                                jlNueveCBlanca.setVisible(false);
                                                            } else {
                                                                jlNueveCNegra.setVisible(false);
                                                            }
                                                        } else {
                                                            if (columna == 3) {
                                                                if (turno) {
                                                                    jlNueveDBlanca.setVisible(false);
                                                                } else {
                                                                    jlNueveDNegra.setVisible(false);
                                                                }
                                                            } else {
                                                                if (columna == 4) {
                                                                    if (turno) {
                                                                        jlNueveEBlanca.setVisible(false);
                                                                    } else {
                                                                        jlNueveENegra.setVisible(false);
                                                                    }
                                                                } else {
                                                                    if (columna == 5) {
                                                                        if (turno) {
                                                                            jlNueveFBlanca.setVisible(false);
                                                                        } else {
                                                                            jlNueveFNegra.setVisible(false);
                                                                        }
                                                                    } else {
                                                                        if (columna == 6) {
                                                                            if (turno) {
                                                                                jlNueveGBlanca.setVisible(false);
                                                                            } else {
                                                                                jlNueveGNegra.setVisible(false);
                                                                            }
                                                                        } else {
                                                                            if (columna == 7) {
                                                                                if (turno) {
                                                                                    jlNueveHBlanca.setVisible(false);
                                                                                } else {
                                                                                    jlNueveHNegra.setVisible(false);
                                                                                }
                                                                            } else {
                                                                                if (columna == 8) {
                                                                                    if (turno) {
                                                                                        jlNueveIBlanca.setVisible(false);
                                                                                    } else {
                                                                                        jlNueveINegra.setVisible(false);
                                                                                    }
                                                                                }
                                                                            }//columna 8 
                                                                        }//columna 7
                                                                    }//columna 6
                                                                }//columna 5
                                                            }//columna 4
                                                        }//columna 3
                                                    }//columna 2
                                                }//columna 1
                                            }//if fila 8
                                        }//else fila 8
                                    }//else fila 7
                                }//else fila 6
                            }//else fila 5
                        }//else fila 4
                    }//else fila 3
                }//else fila 2
            }//else fila 1
            return null;
        }//fin del metodo asignar imagen

        /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jlUnoANegra = new javax.swing.JLabel();
        jlUnoBNegra = new javax.swing.JLabel();
        jlUnoCNegra = new javax.swing.JLabel();
        jlUnoENegra = new javax.swing.JLabel();
        jlUnoDNegra = new javax.swing.JLabel();
        jlUnoFNegra = new javax.swing.JLabel();
        jlUnoGNegra = new javax.swing.JLabel();
        jlUnoHNegra = new javax.swing.JLabel();
        jlUnoINegra = new javax.swing.JLabel();
        jlDosANegra = new javax.swing.JLabel();
        jlDosBNegra = new javax.swing.JLabel();
        jlDosCNegra = new javax.swing.JLabel();
        jlDosDNegra = new javax.swing.JLabel();
        jlDosENegra = new javax.swing.JLabel();
        jlDosFNegra = new javax.swing.JLabel();
        jlDosGNegra = new javax.swing.JLabel();
        jlDosHNegra = new javax.swing.JLabel();
        jlDosINegra = new javax.swing.JLabel();
        jlTresANegra = new javax.swing.JLabel();
        jlTresBNegra = new javax.swing.JLabel();
        jlTresCNegra = new javax.swing.JLabel();
        jlTresDNegra = new javax.swing.JLabel();
        jlTresENegra = new javax.swing.JLabel();
        jlTresFNegra = new javax.swing.JLabel();
        jlTresGNegra = new javax.swing.JLabel();
        jlTresHNegra = new javax.swing.JLabel();
        jlTresINegra = new javax.swing.JLabel();
        jlCuatroANegra = new javax.swing.JLabel();
        jlCuatroBNegra = new javax.swing.JLabel();
        jlCuatroCNegra = new javax.swing.JLabel();
        jlCuatroDNegra = new javax.swing.JLabel();
        jlCuatroENegra = new javax.swing.JLabel();
        jlCuatroFNegra = new javax.swing.JLabel();
        jlCuatroGNegra = new javax.swing.JLabel();
        jlCuatroHNegra = new javax.swing.JLabel();
        jlCuatroINegra = new javax.swing.JLabel();
        jlCincoANegra = new javax.swing.JLabel();
        jlCincoBNegra = new javax.swing.JLabel();
        jlCincoCNegra = new javax.swing.JLabel();
        jlCincoDNegra = new javax.swing.JLabel();
        jlCincoENegra = new javax.swing.JLabel();
        jlCincoFNegra = new javax.swing.JLabel();
        jlCincoGNegra = new javax.swing.JLabel();
        jlCincoHNegra = new javax.swing.JLabel();
        jlCincoINegra = new javax.swing.JLabel();
        jlSeisANegra = new javax.swing.JLabel();
        jlSeisBNegra = new javax.swing.JLabel();
        jlSeisCNegra = new javax.swing.JLabel();
        jlSeisDNegra = new javax.swing.JLabel();
        jlSeisENegra = new javax.swing.JLabel();
        jlSeisFNegra = new javax.swing.JLabel();
        jlSeisGNegra = new javax.swing.JLabel();
        jlSeisHNegra = new javax.swing.JLabel();
        jlSeisINegra = new javax.swing.JLabel();
        jlSieteANegra = new javax.swing.JLabel();
        jlSieteBNegra = new javax.swing.JLabel();
        jlSieteCNegra = new javax.swing.JLabel();
        jlSieteDNegra = new javax.swing.JLabel();
        jlSieteENegra = new javax.swing.JLabel();
        jlSieteFNegra = new javax.swing.JLabel();
        jlSieteGNegra = new javax.swing.JLabel();
        jlSieteHNegra = new javax.swing.JLabel();
        jlSieteINegra = new javax.swing.JLabel();
        jlOchoANegra = new javax.swing.JLabel();
        jlOchoBNegra = new javax.swing.JLabel();
        jlOchoCNegra = new javax.swing.JLabel();
        jlOchoDNegra = new javax.swing.JLabel();
        jlOchoENegra = new javax.swing.JLabel();
        jlOchoFNegra = new javax.swing.JLabel();
        jlOchoGNegra = new javax.swing.JLabel();
        jlOchoHNegra = new javax.swing.JLabel();
        jlOchoINegra = new javax.swing.JLabel();
        jlNueveANegra = new javax.swing.JLabel();
        jlNueveBNegra = new javax.swing.JLabel();
        jlNueveCNegra = new javax.swing.JLabel();
        jlNueveDNegra = new javax.swing.JLabel();
        jlNueveENegra = new javax.swing.JLabel();
        jlNueveFNegra = new javax.swing.JLabel();
        jlNueveGNegra = new javax.swing.JLabel();
        jlNueveHNegra = new javax.swing.JLabel();
        jlNueveINegra = new javax.swing.JLabel();
        jlUnoABlanca = new javax.swing.JLabel();
        jlUnoBBlanca = new javax.swing.JLabel();
        jlUnoCBlanca = new javax.swing.JLabel();
        jlUnoDBlanca = new javax.swing.JLabel();
        jlUnoEBlanca = new javax.swing.JLabel();
        jlUnoFBlanca = new javax.swing.JLabel();
        jlUnoGBlanca = new javax.swing.JLabel();
        jlUnoHBlanca = new javax.swing.JLabel();
        jlUnoIBlanca = new javax.swing.JLabel();
        jlDosABlanca = new javax.swing.JLabel();
        jlDosBBlanca = new javax.swing.JLabel();
        jlDosCBlanca = new javax.swing.JLabel();
        jlDosDBlanca = new javax.swing.JLabel();
        jlDosEBlanca = new javax.swing.JLabel();
        jlDosFBlanca = new javax.swing.JLabel();
        jlDosGBlanca = new javax.swing.JLabel();
        jlDosHBlanca = new javax.swing.JLabel();
        jlDosIBlanca = new javax.swing.JLabel();
        jlTresABlanca = new javax.swing.JLabel();
        jlTresBBlanca = new javax.swing.JLabel();
        jlTresCBlanca = new javax.swing.JLabel();
        jlTresDBlanca = new javax.swing.JLabel();
        jlTresEBlanca = new javax.swing.JLabel();
        jlTresFBlanca = new javax.swing.JLabel();
        jlTresGBlanca = new javax.swing.JLabel();
        jlTresHBlanca = new javax.swing.JLabel();
        jlTresIBlanca = new javax.swing.JLabel();
        jlCuatroABlanca = new javax.swing.JLabel();
        jlCuatroBBlanca = new javax.swing.JLabel();
        jlCuatroCBlanca = new javax.swing.JLabel();
        jlCuatroDBlanca = new javax.swing.JLabel();
        jlCuatroEBlanca = new javax.swing.JLabel();
        jlCuatroFBlanca = new javax.swing.JLabel();
        jlCuatroGBlanca = new javax.swing.JLabel();
        jlCuatroHBlanca = new javax.swing.JLabel();
        jlCuatroIBlanca = new javax.swing.JLabel();
        jlCincoABlanca = new javax.swing.JLabel();
        jlCincoBBlanca = new javax.swing.JLabel();
        jlCincoCBlanca = new javax.swing.JLabel();
        jlCincoDBlanca = new javax.swing.JLabel();
        jlCincoEBlanca = new javax.swing.JLabel();
        jlCincoFBlanca = new javax.swing.JLabel();
        jlCincoGBlanca = new javax.swing.JLabel();
        jlCincoHBlanca = new javax.swing.JLabel();
        jlCincoIBlanca = new javax.swing.JLabel();
        jlSeisABlanca = new javax.swing.JLabel();
        jlSeisBBlanca = new javax.swing.JLabel();
        jlSeisCBlanca = new javax.swing.JLabel();
        jlSeisDBlanca = new javax.swing.JLabel();
        jlSeisEBlanca = new javax.swing.JLabel();
        jlSeisFBlanca = new javax.swing.JLabel();
        jlSeisGBlanca = new javax.swing.JLabel();
        jlSeisHBlanca = new javax.swing.JLabel();
        jlSeisIBlanca = new javax.swing.JLabel();
        jlSieteABlanca = new javax.swing.JLabel();
        jlSieteBBlanca = new javax.swing.JLabel();
        jlSieteCBlanca = new javax.swing.JLabel();
        jlSieteDBlanca = new javax.swing.JLabel();
        jlSieteEBlanca = new javax.swing.JLabel();
        jlSieteFBlanca = new javax.swing.JLabel();
        jlSieteGBlanca = new javax.swing.JLabel();
        jlSieteHBlanca = new javax.swing.JLabel();
        jlSieteIBlanca = new javax.swing.JLabel();
        jlOchoABlanca = new javax.swing.JLabel();
        jlOchoBBlanca = new javax.swing.JLabel();
        jlOchoCBlanca = new javax.swing.JLabel();
        jlOchoDBlanca = new javax.swing.JLabel();
        jlOchoEBlanca = new javax.swing.JLabel();
        jlOchoFBlanca = new javax.swing.JLabel();
        jlOchoGBlanca = new javax.swing.JLabel();
        jlOchoHBlanca = new javax.swing.JLabel();
        jlOchoIBlanca = new javax.swing.JLabel();
        jlNueveABlanca = new javax.swing.JLabel();
        jlNueveBBlanca = new javax.swing.JLabel();
        jlNueveCBlanca = new javax.swing.JLabel();
        jlNueveDBlanca = new javax.swing.JLabel();
        jlNueveEBlanca = new javax.swing.JLabel();
        jlNueveFBlanca = new javax.swing.JLabel();
        jlNueveGBlanca = new javax.swing.JLabel();
        jlNueveHBlanca = new javax.swing.JLabel();
        jlNueveIBlanca = new javax.swing.JLabel();
        jlFondoTablero = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jlUnoANegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlUnoANegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 330, -1, -1));

        jlUnoBNegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlUnoBNegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 330, -1, -1));

        jlUnoCNegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlUnoCNegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 330, -1, -1));

        jlUnoENegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlUnoENegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 330, -1, -1));

        jlUnoDNegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlUnoDNegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 330, -1, -1));

        jlUnoFNegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlUnoFNegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 330, -1, -1));

        jlUnoGNegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlUnoGNegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 330, -1, -1));

        jlUnoHNegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlUnoHNegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 330, -1, -1));

        jlUnoINegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlUnoINegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 330, -1, -1));

        jlDosANegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlDosANegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 370, -1, -1));

        jlDosBNegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlDosBNegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 370, -1, -1));

        jlDosCNegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlDosCNegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 370, -1, -1));

        jlDosDNegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlDosDNegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 370, -1, -1));

        jlDosENegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlDosENegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 370, -1, -1));

        jlDosFNegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlDosFNegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 370, -1, -1));

        jlDosGNegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlDosGNegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 370, -1, -1));

        jlDosHNegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlDosHNegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 370, -1, -1));

        jlDosINegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlDosINegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 370, -1, -1));

        jlTresANegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlTresANegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 420, -1, -1));

        jlTresBNegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlTresBNegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 420, -1, -1));

        jlTresCNegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlTresCNegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 420, -1, -1));

        jlTresDNegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlTresDNegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 420, -1, -1));

        jlTresENegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlTresENegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 420, -1, -1));

        jlTresFNegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlTresFNegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 420, -1, -1));

        jlTresGNegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlTresGNegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 420, -1, -1));

        jlTresHNegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlTresHNegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 420, -1, -1));

        jlTresINegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlTresINegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 420, -1, -1));

        jlCuatroANegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlCuatroANegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 460, -1, -1));

        jlCuatroBNegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlCuatroBNegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 460, -1, -1));

        jlCuatroCNegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlCuatroCNegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 460, -1, -1));

        jlCuatroDNegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlCuatroDNegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 460, -1, -1));

        jlCuatroENegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlCuatroENegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 460, -1, -1));

        jlCuatroFNegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlCuatroFNegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 460, -1, -1));

        jlCuatroGNegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlCuatroGNegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 460, -1, -1));

        jlCuatroHNegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlCuatroHNegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 460, -1, -1));

        jlCuatroINegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlCuatroINegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 460, -1, -1));

        jlCincoANegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlCincoANegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 510, -1, -1));

        jlCincoBNegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlCincoBNegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 510, -1, -1));

        jlCincoCNegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlCincoCNegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 510, -1, -1));

        jlCincoDNegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlCincoDNegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 510, -1, -1));

        jlCincoENegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlCincoENegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 510, -1, -1));

        jlCincoFNegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlCincoFNegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 510, -1, -1));

        jlCincoGNegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlCincoGNegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 510, -1, -1));

        jlCincoHNegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlCincoHNegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 510, -1, -1));

        jlCincoINegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlCincoINegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 510, -1, -1));

        jlSeisANegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlSeisANegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 550, -1, -1));

        jlSeisBNegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlSeisBNegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 550, -1, -1));

        jlSeisCNegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlSeisCNegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 550, -1, -1));

        jlSeisDNegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlSeisDNegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 550, -1, -1));

        jlSeisENegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlSeisENegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 550, -1, -1));

        jlSeisFNegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlSeisFNegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 550, -1, -1));

        jlSeisGNegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlSeisGNegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 550, -1, -1));

        jlSeisHNegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlSeisHNegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 550, -1, -1));

        jlSeisINegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlSeisINegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 550, -1, -1));

        jlSieteANegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlSieteANegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 600, -1, -1));

        jlSieteBNegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlSieteBNegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 600, -1, -1));

        jlSieteCNegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlSieteCNegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 600, -1, -1));

        jlSieteDNegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlSieteDNegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 600, -1, -1));

        jlSieteENegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlSieteENegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 600, -1, -1));

        jlSieteFNegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlSieteFNegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 600, -1, -1));

        jlSieteGNegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlSieteGNegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 600, -1, -1));

        jlSieteHNegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlSieteHNegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 600, -1, -1));

        jlSieteINegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlSieteINegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 600, -1, -1));

        jlOchoANegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlOchoANegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 650, -1, -1));

        jlOchoBNegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlOchoBNegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 650, -1, -1));

        jlOchoCNegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlOchoCNegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 650, -1, -1));

        jlOchoDNegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlOchoDNegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 650, -1, -1));

        jlOchoENegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlOchoENegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 650, -1, -1));

        jlOchoFNegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlOchoFNegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 650, -1, -1));

        jlOchoGNegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlOchoGNegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 650, -1, -1));

        jlOchoHNegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlOchoHNegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 650, -1, -1));

        jlOchoINegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlOchoINegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 650, -1, -1));

        jlNueveANegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlNueveANegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 700, -1, -1));

        jlNueveBNegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlNueveBNegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 700, -1, -1));

        jlNueveCNegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlNueveCNegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 700, -1, -1));

        jlNueveDNegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlNueveDNegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 700, -1, -1));

        jlNueveENegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlNueveENegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 700, -1, -1));

        jlNueveFNegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlNueveFNegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 700, -1, -1));

        jlNueveGNegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlNueveGNegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 700, -1, -1));

        jlNueveHNegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlNueveHNegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 700, -1, -1));

        jlNueveINegra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraNegra.png"))); // NOI18N
        getContentPane().add(jlNueveINegra, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 700, -1, -1));

        jlUnoABlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlUnoABlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 340, 50, 40));

        jlUnoBBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlUnoBBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 340, 50, 40));

        jlUnoCBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlUnoCBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 340, 50, 40));

        jlUnoDBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlUnoDBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 340, 50, 40));

        jlUnoEBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlUnoEBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 340, 50, 40));

        jlUnoFBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlUnoFBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 340, 50, 40));

        jlUnoGBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlUnoGBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 340, 50, 40));

        jlUnoHBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlUnoHBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 340, 50, 40));

        jlUnoIBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlUnoIBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 340, 50, 40));

        jlDosABlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlDosABlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 380, 50, 40));

        jlDosBBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlDosBBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 380, 50, 40));

        jlDosCBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlDosCBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 380, 50, 40));

        jlDosDBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlDosDBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 380, 50, 40));

        jlDosEBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlDosEBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 380, 50, 40));

        jlDosFBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlDosFBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 380, 50, 40));

        jlDosGBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlDosGBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 380, 50, 40));

        jlDosHBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlDosHBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 380, 50, 40));

        jlDosIBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlDosIBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 380, 50, 40));

        jlTresABlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlTresABlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 430, 50, 40));

        jlTresBBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlTresBBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 430, 50, 40));

        jlTresCBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlTresCBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 430, 50, 40));

        jlTresDBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlTresDBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 430, 50, 40));

        jlTresEBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlTresEBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 430, 50, 40));

        jlTresFBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlTresFBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 430, 50, 40));

        jlTresGBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlTresGBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 430, 50, 40));

        jlTresHBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlTresHBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 430, 50, 40));

        jlTresIBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlTresIBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 430, 50, 40));

        jlCuatroABlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlCuatroABlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 470, 50, 40));

        jlCuatroBBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlCuatroBBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 470, 50, 40));

        jlCuatroCBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlCuatroCBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 470, 50, 40));

        jlCuatroDBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlCuatroDBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 470, 50, 40));

        jlCuatroEBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlCuatroEBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 470, 50, 40));

        jlCuatroFBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlCuatroFBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 470, 50, 40));

        jlCuatroGBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlCuatroGBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 470, 50, 40));

        jlCuatroHBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlCuatroHBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 470, 50, 40));

        jlCuatroIBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlCuatroIBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 470, 50, 40));

        jlCincoABlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlCincoABlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 520, 50, 40));

        jlCincoBBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlCincoBBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 520, 50, 40));

        jlCincoCBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlCincoCBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 520, 50, 40));

        jlCincoDBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlCincoDBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 520, 50, 40));

        jlCincoEBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlCincoEBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 520, 50, 40));

        jlCincoFBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlCincoFBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 520, 50, 40));

        jlCincoGBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlCincoGBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 520, 50, 40));

        jlCincoHBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlCincoHBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 520, 50, 40));

        jlCincoIBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlCincoIBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 520, 50, 40));

        jlSeisABlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlSeisABlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 560, 50, 40));

        jlSeisBBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlSeisBBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 560, 50, 40));

        jlSeisCBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlSeisCBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 560, 40, 40));

        jlSeisDBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlSeisDBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 560, 50, 40));

        jlSeisEBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlSeisEBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 560, 50, 40));

        jlSeisFBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlSeisFBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 560, 50, 40));

        jlSeisGBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlSeisGBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 560, 50, 40));

        jlSeisHBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlSeisHBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 560, 50, 40));

        jlSeisIBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlSeisIBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 560, 50, 40));

        jlSieteABlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlSieteABlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 610, 50, 40));

        jlSieteBBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlSieteBBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 610, 50, 40));

        jlSieteCBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlSieteCBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 600, 50, -1));

        jlSieteDBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlSieteDBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 610, 50, 40));

        jlSieteEBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlSieteEBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 610, 50, 40));

        jlSieteFBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlSieteFBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 610, 50, 40));

        jlSieteGBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlSieteGBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 610, 50, 40));

        jlSieteHBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlSieteHBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 610, 50, 40));

        jlSieteIBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlSieteIBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 610, 50, 40));

        jlOchoABlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlOchoABlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 660, 50, 40));

        jlOchoBBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlOchoBBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 660, 50, 40));

        jlOchoCBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlOchoCBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 660, 50, 40));

        jlOchoDBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlOchoDBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 660, 50, 40));

        jlOchoEBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlOchoEBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 660, 50, 40));

        jlOchoFBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlOchoFBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 660, 50, 40));

        jlOchoGBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlOchoGBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 660, 50, 40));

        jlOchoHBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlOchoHBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 660, 50, 40));

        jlOchoIBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlOchoIBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 660, 50, 40));

        jlNueveABlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlNueveABlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 710, 50, 40));

        jlNueveBBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlNueveBBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 710, 50, 40));

        jlNueveCBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlNueveCBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 710, 50, 40));

        jlNueveDBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlNueveDBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 710, 60, 40));

        jlNueveEBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlNueveEBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 710, 50, 40));

        jlNueveFBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlNueveFBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 710, -1, 40));

        jlNueveGBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlNueveGBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 710, 50, 40));

        jlNueveHBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlNueveHBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 710, 50, 40));

        jlNueveIBlanca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PiedraBlanca.png"))); // NOI18N
        getContentPane().add(jlNueveIBlanca, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 710, 50, 40));

        jlFondoTablero.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/imagenTablero.jpg"))); // NOI18N
        getContentPane().add(jlFondoTablero, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 560, 800));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    /* public static void main(String args[]) {
        /* Set the Nimbus look and feel */
    //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
    /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
     */
 /*try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JFImagenFondo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JFImagenFondo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JFImagenFondo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JFImagenFondo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
 /* java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new JFImagenFondo().setVisible(true);
            }
        });
        
        
    }*/
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jlCincoABlanca;
    private javax.swing.JLabel jlCincoANegra;
    private javax.swing.JLabel jlCincoBBlanca;
    private javax.swing.JLabel jlCincoBNegra;
    private javax.swing.JLabel jlCincoCBlanca;
    private javax.swing.JLabel jlCincoCNegra;
    private javax.swing.JLabel jlCincoDBlanca;
    private javax.swing.JLabel jlCincoDNegra;
    private javax.swing.JLabel jlCincoEBlanca;
    private javax.swing.JLabel jlCincoENegra;
    private javax.swing.JLabel jlCincoFBlanca;
    private javax.swing.JLabel jlCincoFNegra;
    private javax.swing.JLabel jlCincoGBlanca;
    private javax.swing.JLabel jlCincoGNegra;
    private javax.swing.JLabel jlCincoHBlanca;
    private javax.swing.JLabel jlCincoHNegra;
    private javax.swing.JLabel jlCincoIBlanca;
    private javax.swing.JLabel jlCincoINegra;
    private javax.swing.JLabel jlCuatroABlanca;
    private javax.swing.JLabel jlCuatroANegra;
    private javax.swing.JLabel jlCuatroBBlanca;
    private javax.swing.JLabel jlCuatroBNegra;
    private javax.swing.JLabel jlCuatroCBlanca;
    private javax.swing.JLabel jlCuatroCNegra;
    private javax.swing.JLabel jlCuatroDBlanca;
    private javax.swing.JLabel jlCuatroDNegra;
    private javax.swing.JLabel jlCuatroEBlanca;
    private javax.swing.JLabel jlCuatroENegra;
    private javax.swing.JLabel jlCuatroFBlanca;
    private javax.swing.JLabel jlCuatroFNegra;
    private javax.swing.JLabel jlCuatroGBlanca;
    private javax.swing.JLabel jlCuatroGNegra;
    private javax.swing.JLabel jlCuatroHBlanca;
    private javax.swing.JLabel jlCuatroHNegra;
    private javax.swing.JLabel jlCuatroIBlanca;
    private javax.swing.JLabel jlCuatroINegra;
    private javax.swing.JLabel jlDosABlanca;
    private javax.swing.JLabel jlDosANegra;
    private javax.swing.JLabel jlDosBBlanca;
    private javax.swing.JLabel jlDosBNegra;
    private javax.swing.JLabel jlDosCBlanca;
    private javax.swing.JLabel jlDosCNegra;
    private javax.swing.JLabel jlDosDBlanca;
    private javax.swing.JLabel jlDosDNegra;
    private javax.swing.JLabel jlDosEBlanca;
    private javax.swing.JLabel jlDosENegra;
    private javax.swing.JLabel jlDosFBlanca;
    private javax.swing.JLabel jlDosFNegra;
    private javax.swing.JLabel jlDosGBlanca;
    private javax.swing.JLabel jlDosGNegra;
    private javax.swing.JLabel jlDosHBlanca;
    private javax.swing.JLabel jlDosHNegra;
    private javax.swing.JLabel jlDosIBlanca;
    private javax.swing.JLabel jlDosINegra;
    private javax.swing.JLabel jlFondoTablero;
    private javax.swing.JLabel jlNueveABlanca;
    private javax.swing.JLabel jlNueveANegra;
    private javax.swing.JLabel jlNueveBBlanca;
    private javax.swing.JLabel jlNueveBNegra;
    private javax.swing.JLabel jlNueveCBlanca;
    private javax.swing.JLabel jlNueveCNegra;
    private javax.swing.JLabel jlNueveDBlanca;
    private javax.swing.JLabel jlNueveDNegra;
    private javax.swing.JLabel jlNueveEBlanca;
    private javax.swing.JLabel jlNueveENegra;
    private javax.swing.JLabel jlNueveFBlanca;
    private javax.swing.JLabel jlNueveFNegra;
    private javax.swing.JLabel jlNueveGBlanca;
    private javax.swing.JLabel jlNueveGNegra;
    private javax.swing.JLabel jlNueveHBlanca;
    private javax.swing.JLabel jlNueveHNegra;
    private javax.swing.JLabel jlNueveIBlanca;
    private javax.swing.JLabel jlNueveINegra;
    private javax.swing.JLabel jlOchoABlanca;
    private javax.swing.JLabel jlOchoANegra;
    private javax.swing.JLabel jlOchoBBlanca;
    private javax.swing.JLabel jlOchoBNegra;
    private javax.swing.JLabel jlOchoCBlanca;
    private javax.swing.JLabel jlOchoCNegra;
    private javax.swing.JLabel jlOchoDBlanca;
    private javax.swing.JLabel jlOchoDNegra;
    private javax.swing.JLabel jlOchoEBlanca;
    private javax.swing.JLabel jlOchoENegra;
    private javax.swing.JLabel jlOchoFBlanca;
    private javax.swing.JLabel jlOchoFNegra;
    private javax.swing.JLabel jlOchoGBlanca;
    private javax.swing.JLabel jlOchoGNegra;
    private javax.swing.JLabel jlOchoHBlanca;
    private javax.swing.JLabel jlOchoHNegra;
    private javax.swing.JLabel jlOchoIBlanca;
    private javax.swing.JLabel jlOchoINegra;
    private javax.swing.JLabel jlSeisABlanca;
    private javax.swing.JLabel jlSeisANegra;
    private javax.swing.JLabel jlSeisBBlanca;
    private javax.swing.JLabel jlSeisBNegra;
    private javax.swing.JLabel jlSeisCBlanca;
    private javax.swing.JLabel jlSeisCNegra;
    private javax.swing.JLabel jlSeisDBlanca;
    private javax.swing.JLabel jlSeisDNegra;
    private javax.swing.JLabel jlSeisEBlanca;
    private javax.swing.JLabel jlSeisENegra;
    private javax.swing.JLabel jlSeisFBlanca;
    private javax.swing.JLabel jlSeisFNegra;
    private javax.swing.JLabel jlSeisGBlanca;
    private javax.swing.JLabel jlSeisGNegra;
    private javax.swing.JLabel jlSeisHBlanca;
    private javax.swing.JLabel jlSeisHNegra;
    private javax.swing.JLabel jlSeisIBlanca;
    private javax.swing.JLabel jlSeisINegra;
    private javax.swing.JLabel jlSieteABlanca;
    private javax.swing.JLabel jlSieteANegra;
    private javax.swing.JLabel jlSieteBBlanca;
    private javax.swing.JLabel jlSieteBNegra;
    private javax.swing.JLabel jlSieteCBlanca;
    private javax.swing.JLabel jlSieteCNegra;
    private javax.swing.JLabel jlSieteDBlanca;
    private javax.swing.JLabel jlSieteDNegra;
    private javax.swing.JLabel jlSieteEBlanca;
    private javax.swing.JLabel jlSieteENegra;
    private javax.swing.JLabel jlSieteFBlanca;
    private javax.swing.JLabel jlSieteFNegra;
    private javax.swing.JLabel jlSieteGBlanca;
    private javax.swing.JLabel jlSieteGNegra;
    private javax.swing.JLabel jlSieteHBlanca;
    private javax.swing.JLabel jlSieteHNegra;
    private javax.swing.JLabel jlSieteIBlanca;
    private javax.swing.JLabel jlSieteINegra;
    private javax.swing.JLabel jlTresABlanca;
    private javax.swing.JLabel jlTresANegra;
    private javax.swing.JLabel jlTresBBlanca;
    private javax.swing.JLabel jlTresBNegra;
    private javax.swing.JLabel jlTresCBlanca;
    private javax.swing.JLabel jlTresCNegra;
    private javax.swing.JLabel jlTresDBlanca;
    private javax.swing.JLabel jlTresDNegra;
    private javax.swing.JLabel jlTresEBlanca;
    private javax.swing.JLabel jlTresENegra;
    private javax.swing.JLabel jlTresFBlanca;
    private javax.swing.JLabel jlTresFNegra;
    private javax.swing.JLabel jlTresGBlanca;
    private javax.swing.JLabel jlTresGNegra;
    private javax.swing.JLabel jlTresHBlanca;
    private javax.swing.JLabel jlTresHNegra;
    private javax.swing.JLabel jlTresIBlanca;
    private javax.swing.JLabel jlTresINegra;
    private javax.swing.JLabel jlUnoABlanca;
    private javax.swing.JLabel jlUnoANegra;
    private javax.swing.JLabel jlUnoBBlanca;
    private javax.swing.JLabel jlUnoBNegra;
    private javax.swing.JLabel jlUnoCBlanca;
    private javax.swing.JLabel jlUnoCNegra;
    private javax.swing.JLabel jlUnoDBlanca;
    private javax.swing.JLabel jlUnoDNegra;
    private javax.swing.JLabel jlUnoEBlanca;
    private javax.swing.JLabel jlUnoENegra;
    private javax.swing.JLabel jlUnoFBlanca;
    private javax.swing.JLabel jlUnoFNegra;
    private javax.swing.JLabel jlUnoGBlanca;
    private javax.swing.JLabel jlUnoGNegra;
    private javax.swing.JLabel jlUnoHBlanca;
    private javax.swing.JLabel jlUnoHNegra;
    private javax.swing.JLabel jlUnoIBlanca;
    private javax.swing.JLabel jlUnoINegra;
    // End of variables declaration//GEN-END:variables
}
