/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.diana.controlador;

import cr.diana.modelo.EjesTableroGrafico;
import cr.diana.modelo.EstadisticasGenerales;
import cr.diana.modelo.MovimientosPermitidos;
import cr.diana.modelo.Partida;
import cr.diana.modelo.Piedra;
import cr.diana.modelo.Tablero;
import cr.diana.vista.JFImagenFondo;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 *
 * @author Dianis
 */
public class Manejador implements MouseListener {

    private JFImagenFondo imagenes;
    private EjesTableroGrafico ejes;
    private Tablero tablero;
    private MovimientosPermitidos movimientosPermitidos;
    private Piedra piedra;
    private Partida partida;
    private EstadisticasGenerales estadisticas;

    public Manejador(Tablero tablero, Partida partida) {
        //this.imagenFondo= imagenFondo;
        this.tablero = tablero;
        this.partida = partida;
        movimientosPermitidos = new MovimientosPermitidos(tablero, imagenes, partida);
        ejes = new EjesTableroGrafico();
    }//fin del constructor

    @Override
    public void mouseClicked(MouseEvent evento) {

        if (ejes.rangoY(evento.getX()) != -1 & ejes.rangoX(evento.getY()) != -1) {
            if (tablero.comprobarVacio(ejes.rangoX(evento.getY()), ejes.rangoY(evento.getX()))) {
                if (partida.getTurno()) {
                    piedra = new Piedra("Blanca");
                    tablero.setPiedra(piedra, ejes.rangoX(evento.getY()), ejes.rangoY(evento.getX()));
                    System.out.println("Eje Y: " + evento.getY() + " Eje x: " + evento.getX());
                    imagenes.asignarImagen(ejes.rangoX(evento.getY()), ejes.rangoY(evento.getX()), partida);
                    partida.setTurno(false);
                    
                    if (tablero.esEsquina(ejes.rangoX(evento.getY()), ejes.rangoY(evento.getX()))) {
                        movimientosPermitidos.espacioRodeadoEsquina(ejes.rangoX(evento.getY()), ejes.rangoY(evento.getX()));
                    }
                    if (tablero.esBordeArriba(ejes.rangoX(evento.getY()), ejes.rangoY(evento.getX()))) {
                        movimientosPermitidos.espacioRodeadoBordeArriba(ejes.rangoX(evento.getY()), ejes.rangoY(evento.getX()));
                    }
                    if (tablero.esBordeAbajo(ejes.rangoX(evento.getY()), ejes.rangoY(evento.getX()))) {
                        movimientosPermitidos.espacioRodeadoBordeAbajo(ejes.rangoX(evento.getY()), ejes.rangoY(evento.getX()));
                    }
                    if (tablero.esBordeDerecha(ejes.rangoX(evento.getY()), ejes.rangoY(evento.getX()))) {
                        movimientosPermitidos.espacioRodeadoBordeDerecho(ejes.rangoX(evento.getY()), ejes.rangoY(evento.getX()));
                    }
                    if (tablero.esBordeIzquierda(ejes.rangoX(evento.getY()), ejes.rangoY(evento.getX()))) {
                        movimientosPermitidos.espacioRodeadoBordeIzquierdo(ejes.rangoX(evento.getY()), ejes.rangoY(evento.getX()));
                    } else {
                        movimientosPermitidos.espacioRodeadoCentral(ejes.rangoX(evento.getY()), ejes.rangoY(evento.getX()));
                    }
                    
                } else {
                    piedra = new Piedra("Negra");
                    tablero.setPiedra(piedra, ejes.rangoX(evento.getY()), ejes.rangoY(evento.getX()));
                    imagenes.asignarImagen(ejes.rangoX(evento.getY()), ejes.rangoY(evento.getX()), partida);
                    partida.setTurno(true);
                    
                    if (tablero.esEsquina(ejes.rangoX(evento.getY()), ejes.rangoY(evento.getX()))) {
                        movimientosPermitidos.espacioRodeadoEsquina(ejes.rangoX(evento.getY()), ejes.rangoY(evento.getX()));
                    }
                    if (tablero.esBordeArriba(ejes.rangoX(evento.getY()), ejes.rangoY(evento.getX()))) {
                        movimientosPermitidos.espacioRodeadoBordeArriba(ejes.rangoX(evento.getY()), ejes.rangoY(evento.getX()));
                    }
                    if (tablero.esBordeAbajo(ejes.rangoX(evento.getY()), ejes.rangoY(evento.getX()))) {
                        movimientosPermitidos.espacioRodeadoBordeAbajo(ejes.rangoX(evento.getY()), ejes.rangoY(evento.getX()));
                    }
                    if (tablero.esBordeDerecha(ejes.rangoX(evento.getY()), ejes.rangoY(evento.getX()))) {
                        movimientosPermitidos.espacioRodeadoBordeDerecho(ejes.rangoX(evento.getY()), ejes.rangoY(evento.getX()));
                    }
                    if (tablero.esBordeIzquierda(ejes.rangoX(evento.getY()), ejes.rangoY(evento.getX()))) {
                        movimientosPermitidos.espacioRodeadoBordeIzquierdo(ejes.rangoX(evento.getY()), ejes.rangoY(evento.getX()));
                    } else {
                        movimientosPermitidos.espacioRodeadoCentral(ejes.rangoX(evento.getY()), ejes.rangoY(evento.getX()));
                    }
                    
                }
            }
        }
        
       // if(evento.getX()<)
        
        System.out.println(tablero + " " + evento.getX() + " " + evento.getY() + " ");
    }//fin del metodo

    @Override
public void mousePressed(MouseEvent evento) {

    }

    @Override
public void mouseReleased(MouseEvent evento) {

    }

    @Override
public void mouseEntered(MouseEvent evento) {

    }

    @Override
public void mouseExited(MouseEvent evento) {

    }

    public void instanciarImagenes(JFImagenFondo imagenes) {
        this.imagenes = imagenes;
    }

}
