//clase Piedra para el proyecto programado del Juego Go - Fundamentos de programacion ITM
import java.awt.Graphics;
import javax.swing.Icon;
public class Piedra{

	private int x, y;
	private String color;
	private Icon imagen;
	
	public Piedra(String color, Icon imagen){
		setColor(color);
		setImagen(imagen);
		//setX(x);
		//setY(y);
	    }//fin metodo constructor

	public void paint(Graphics g){
		//completar 
		//la posición del rango que vamos a definir segun el tamaño de la imagen de la piedra 
		}
	
	public void setColor(String color){
		this.color = color;
		}//fin metodo 

	public String getColor(){
		return color;
		}

	public void setX(int x){
		this.x = x;
		}
		
	public int getX(){
		return x;
		}
		
	public void setY(int y){
		this.y = y;
	    }
		
	public int getY(){
		return y;
		}
		
	public void setImagen(Icon imagen){
		this.imagen = imagen;
		}
		
	public Icon getImagen(){
		return imagen;
		}
		
	public String toString(){
		return getColor();
	}
}//fin clase Piedra
